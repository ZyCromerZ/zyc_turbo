#!/system/bin/sh
#
# Copyright (C) 2021 ZyCromerZ
# SPDX-License-Identifier: GPL-3.0-or-later
#
for GetAllCmds in ${@}
do
    eval $GetAllCmds
done

. $ModulPath/system/etc/ZyC-Core/misc/initialize.sh
. $ModulPath/system/etc/ZyC-Core/misc/key.sh 
. $ModulPath/system/etc/ZyC-Core/misc/funclist.sh

sync

while DDC "$MODULE_STATUS" == "1" ]]
do
    # always check module status
    RunModules="$(cat /system/etc/ZyC-Core/configs/status.conf)"
    if DDC "$RunModules" == "1" ]];then
        if DDC "$BOOTmode" == "1" ]];then
            . $MPATH/system/etc/ZyC-Core/main.sh "boot"
            BOOTmode="0"
        elif DDC "$BOOTmode" == "0" ]];then
            . $MPATH/system/etc/ZyC-Core/main.sh
        fi
    else
        for ngentod in last_fstrim.log last_optimize_database.log logs.log logs_error.log optimize_database.log write.log;do
            [[ "$(cat $MPATH/system/etc/ZyC-Core/info/$ngentod)" != *"please update status.conf to 1 for run modules"* ]] && echo "please update status.conf to 1 for run modules" > $MPATH/system/etc/ZyC-Core/info/$ngentod
        done
        sleep 10s
    fi
done
