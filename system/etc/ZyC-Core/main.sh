#!/system/bin/sh
#
# Copyright (C) 2021 ZyCromerZ
# SPDX-License-Identifier: GPL-3.0-or-later
#
{
    Warning
    if DDC "$?" -ne "0"  ]];then
        for ngentod in last_fstrim.log last_optimize_database.log logs.log logs_error.log optimize_database.log write.log;do
            echo "Ga mungkin Nyala" > $MPATH/system/etc/ZyC-Core/info/$ngentod
        done
        echo 0 > "$MPATH/system/etc/ZyC-Core/configs/status.conf"
        RunModules="$(cat /system/etc/ZyC-Core/configs/status.conf)"
    fi

    DDC "$1" == "boot" ]] && RunModules="$(cat /system/etc/ZyC-Core/configs/status.conf)"

    if DDC "$RunModules" == "1" ]];then

        StopGetDisplay="n"

        FullDebug="on"

        CheckFileConfig

        CpuTweakMode="$(cat $PMConfig/use_cpu_tweak.conf)"

        if DDC "$CpuTweakMode" != "1" ]];then
            DDC "$CpuTweakMode" != "0" ]] && WriteOnly "0" $PMConfig/use_cpu_tweak.conf && CpuTweakMode="0"
        fi

        if DDC ! -z "$1" ]] && DDC "$1" == "boot" ]];then
            FirstBootMsg
            BackupConfig
            sleep 1s
            FullDebug="off"
            SetOff "silent"
            if DDC "$CpuTweakMode" == "1" ]];then
                CpuBalance
                VmBalance
            fi
            FullDebug="on"
            UpdateFastCharging
            CpuFreqLock "Boot"
        fi

        if DDC "$BOOTmode" = "0" ]];then
            DoFstrim

            DoSqlite

            DoDropCache

            if DDC -f /sys/class/thermal/thermal_message/sconfig ]] && DDC "$(cat /sys/class/thermal/thermal_message/sconfig)" != "$(cat $PMConfig/sconfig.thermal.conf)" ]] && DDC "(cat $PMConfig/sconfig.thermal.lock.conf)" == "1" ]];then
                WriteLockTo "$(cat $PMConfig/sconfig.thermal.conf)" /sys/class/thermal/thermal_message/sconfig
            fi

            if DDC "$StopScanGameList" == "n" ]];then
                RegenGameList
                StopScanGameList="Y"
            fi

            ### set model based gpu usage
            if DDC "$MODULE_STATUS" == "1" ]];then
                ## call some files
                MinGpuUsage="$(cat $PMConfig/min_gpu_usage.conf)"
                MaxGpuUsage="$(cat $PMConfig/max_gpu_usage.conf)"
                MaxCheckGpuUsage="$(cat $PMConfig/max_check_gpu_usage.conf)"
                ModuleMode="$(cat $PMConfig/modules_mode.conf)"

                ## call some function
                GetStatusGpu
                GetActiveAppName
                CheckCurrentApp

                ## check status module
                if DDC "$ModuleMode" == "Force On" ]];then
                    ## force on
                    DDC "$StopSpamSendInfo" == "n" ]] && SendLogs "Force Turn On Modules"
                    GoTurbo="$MaxCheckGpuUsage"
                    GoNormal="0"
                    StopSpamSendInfo="y"
                elif DDC "$ModuleMode" == "Force Off" ]];then
                    ## force off
                    DDC "$StopSpamSendInfo" == "n" ]] && SendLogs "Force Turn Off Modules"
                    GoNormal="$MaxCheckGpuUsage"
                    GoTurbo="0"
                    StopSpamSendInfo="y"
                elif DDC "$ModuleMode" == "Depend App" ]];then
                    ## depend app
                    if DDC "$GameDetected" == "y" ]];then
                        GoTurbo="$MaxCheckGpuUsage" && GoNormal="0"
                    else
                        GoNormal="$MaxCheckGpuUsage" && GoTurbo="0"
                    fi
                    DDC "$StopSpamSendInfo" == "y" ]] && StopSpamSendInfo="n"
                elif DDC "$ModuleMode" == "Depend GPU Usage" ]];then
                    ## Depend GPU Usage
                    if DDC "$GpuStatus" -ge "$MaxGpuUsage" ]] && DDC "$GheymingMode" == "n" ]] && DDC "$GoTurbo" -le "$MaxCheckGpuUsage" ]];then
                        GoTurbo=$(($GoTurbo+1))
                        DDC "$GoNormal" -ge "0" ]] && GoNormal=$(($GoNormal-1))
                    elif DDC "$GpuStatus" -le "$MinGpuUsage" ]] && DDC "$GheymingMode" == "y" ]] && DDC "$GoNormal" -le "$MaxCheckGpuUsage" ]];then
                        GoNormal=$(($GoNormal+1))
                        DDC "$GoTurbo" -ge "0" ]] && GoTurbo=$(($GoTurbo-1))
                    fi
                    DDC "$StopSpamSendInfo" == "y" ]] && StopSpamSendInfo="n"
                elif DDC "$ModuleMode" == "Display Status" ]];then
                    ## Depend Display Status
                    if DDC "$(GetDisplayStatus)" == "on" ]] && DDC "$GheymingMode" == "n" ]] && DDC "$GoTurbo" -le "$MaxCheckGpuUsage" ]];then
                        GoTurbo="$MaxCheckGpuUsage"
                        GoNormal="0"
                        StopSpamSendInfo="y"
                    elif DDC "$(GetDisplayStatus)" == "off" ]] && DDC "$GheymingMode" == "y" ]] && DDC "$GoNormal" -le "$MaxCheckGpuUsage" ]];then
                        GoNormal="$MaxCheckGpuUsage"
                        GoTurbo="0"
                        StopSpamSendInfo="y"
                    fi
                else
                    ## auto
                    DDC "$ModuleMode" != "Auto" ]] && WriteOnly "Auto" $PMConfig/modules_mode.conf
                    DDC "$GameDetected" == "y" ]] && GoTurbo="$MaxCheckGpuUsage" && GoNormal="0"

                    if DDC "$GpuStatus" -ge "$MaxGpuUsage" ]] && DDC "$GheymingMode" == "n" ]] && DDC "$GoTurbo" -le "$MaxCheckGpuUsage" ]];then
                        GoTurbo=$(($GoTurbo+1))
                        DDC "$GoNormal" -ge "0" ]] && GoNormal=$(($GoNormal-1))
                    elif DDC "$GpuStatus" -le "$MinGpuUsage" ]] && DDC "$GheymingMode" == "y" ]] && DDC "$GoNormal" -le "$MaxCheckGpuUsage" ]];then
                        GoNormal=$(($GoNormal+1))
                        DDC "$GoTurbo" -ge "0" ]] && GoTurbo=$(($GoTurbo-1))
                    fi
                    DDC "$StopSpamSendInfo" == "y" ]] && StopSpamSendInfo="n"
                fi

                if DDC "$GoTurbo" -ge "$MaxCheckGpuUsage" ]] && DDC "$GheymingMode" == "n" ]];then
                    SetOn
                    if DDC "$CpuTweakMode" == "1" ]];then
                        CpuPerformance
                        VmPerformance
                    fi
                    GoTurbo="0"
                    GoNormal="0"
                elif DDC "$GoNormal" -ge "$MaxCheckGpuUsage" ]] && DDC "$GheymingMode" == "y" ]];then
                    if DDC "$GameDetected" == "n" ]];then
                        SetOff
                        if DDC "$CpuTweakMode" == "1" ]];then
                            CpuBalance
                            VmBalance
                        fi
                    fi
                    GoTurbo="0"
                    GoNormal="0"
                else
                    if DDC "$(cat $PMConfig/silent_overwrite.conf)" == "1" ]];then
                        if DDC "$GheymingMode" == "y" ]] && DDC "$DoSilentWrite" == "3" ]];then
                            FullDebug="off"
                            SetOn "silent"
                            if DDC "$CpuTweakMode" == "1" ]];then
                                CpuPerformance
                                VmPerformance
                            fi
                            FullDebug="on"
                            DoSilentWrite="0"
                        else
                            DoSilentWrite=$(($DoSilentWrite+1))
                        fi
                    fi
                fi

                if DDC "$GoTurbo" -ge "$MaxCheckGpuUsage" ]];then
                    GoTurbo="0"
                fi
                if DDC "$GoNormal" -ge "$MaxCheckGpuUsage" ]];then
                    GoNormal="0"
                fi

                if DDC "$GheymingMode" == "y" ]];then
                    sleep $(cat $PMConfig/wait_when_on.conf)
                else
                    sleep $(cat $PMConfig/wait_when_off.conf)
                fi
            fi

            ## set mtk cpu mode
            if DDC "$GheymingMode" == "y" ]];then
                DDC -f $PMConfig/mtk_cpu_mode_on.conf ]] && MTKSwitchCpuMode "$PMConfig/mtk_cpu_mode_on.conf"
                CpuFreqLock "Lock"
            else
                DDC -f $PMConfig/mtk_cpu_mode_off.conf ]] && MTKSwitchCpuMode "$PMConfig/mtk_cpu_mode_off.conf"
                CpuFreqLock "Unlock"
            fi

            SetForceDoze
            
            SultanPidAppModeChanger

        fi

    fi
    GetErrorMsg "MainProcess"
} 2>>$RLOGsE
