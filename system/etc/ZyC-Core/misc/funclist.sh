#!/system/bin/sh
#
# Copyright (C) 2021 ZyCromerZ
# SPDX-License-Identifier: GPL-3.0-or-later
#
GetErrorMsg()
{
    if DDC "$(cat $PMConfig/show_error.conf)" == "1" ]];then
        RLOGsE=$MPATH/system/etc/ZyC-Core/info/logs_error.log
        local ErrCode="$?"
        DDC "$ErrCode" != "0" ]] && echo "$( date +"%H:%M:%S") | error on function ${1} | Error Code $ErrCode" >>$RLOGsE
    else
        RLOGsE="/dev/null"
        if DDC -f $MPATH/system/etc/ZyC-Core/info/logs_error.log ]];then
            DDC "$(cat $MPATH/system/etc/ZyC-Core/info/logs_error.log)" != *"error logs disabled"* ]] && WriteOnly "error logs disabled" $MPATH/system/etc/ZyC-Core/info/logs_error.log
        else
            WriteOnly "error logs disabled" $MPATH/system/etc/ZyC-Core/info/logs_error.log
        fi
    fi
}

SendLogs(){
    echo "$( date +"%H:%M:%S") | ${1}" >>$LOGs
} 2>>$RLOGsE

SendWLogs(){
    if DDC "$(cat $PMConfig/write_info.conf)" == "1" ]];then
        if DDC "$(cat $PMConfig/write_info.conf)" == "disabled" ]];then
            echo "$( date +"%H:%M:%S") | ${1}" >$MPATH/system/etc/ZyC-Core/info/write.log
        else
            echo "$( date +"%H:%M:%S") | ${1}" >>$MPATH/system/etc/ZyC-Core/info/write.log
        fi
    else
        local TheTEXT="write logs disabled, please enable it first"
        DDC "$(cat $PMConfig/write_info.conf)" != *"$TheTEXT"* ]] && WriteOnly "$TheTEXT" $MPATH/system/etc/ZyC-Core/info/write.log
    fi
} 2>>$RLOGsE

BackupScheduler()
{
    local SchedList=""
    local BlockSD
    for BlockSD in $(ls /sys/block | grep sd)
    do
        if DDC "${#BlockSD}" == "3" ]];then
            SchedList="${SchedList}$(cat /sys/block/$BlockSD/queue/scheduler | awk -F '[' '{print $2}' | awk -F ']' '{print $1}') "
        fi
    done
    DDC ! -z "$SchedList" ]] && WriteOnly "$SchedList" "$PMConfig/backup/sdx_scheduler"
    SchedList=""
    for BlockSD in $(ls /sys/block | grep mmcblk)
    do
        if DDC "${#BlockSD}" == "7" ]];then
            SchedList="${SchedList}$(cat /sys/block/$BlockSD/queue/scheduler | awk -F '[' '{print $2}' | awk -F ']' '{print $1}') "
        fi
    done
    DDC ! -z "$SchedList" ]] && WriteOnly "$SchedList" "$PMConfig/backup/mmcblkx_scheduler"
    GetErrorMsg "BackupScheduler"
} 2>>$RLOGsE

RestoreScheduler()
{
    local TBNumber="1"
    local GetSchedVal=""
    local BlockSD
    for BlockSD in $(ls /sys/block | grep sd)
    do
        if DDC "${#BlockSD}" == "3" ]];then
            if DDC "$(cat $PMConfig/scheduler.lock.conf)" == "0" ]];then
                GetSchedVal="$(cat $PMConfig/backup/sdx_scheduler | awk -F ' ' '{print $'$TBNumber'}' )"
            else
                GetSchedVal="$(cat $PMConfig/scheduler.conf)"
            fi
            WriteTo "$GetSchedVal" /sys/block/${BlockSD}/queue/scheduler
            TBNumber=$(($TBNumber+1))
        fi
    done
    TBNumber="1"
    for BlockSD in $(ls /sys/block | grep mmcblk)
    do
        if DDC "${#BlockSD}" == "7" ]];then
            if DDC "$(cat $PMConfig/scheduler.lock.conf)" == "0" ]];then
                GetSchedVal="$(cat $PMConfig/backup/mmcblkx_scheduler | awk -F ' ' '{print $'$TBNumber'}' )"
            else
                GetSchedVal="$(cat $PMConfig/scheduler.conf)"
            fi
            WriteTo "$GetSchedVal" /sys/block/${BlockSD}/queue/scheduler
            TBNumber=$(($TBNumber+1))
        fi
    done
    GetErrorMsg "RestoreScheduler"
} 2>>$RLOGsE

SetScheduler()
{
    local BlockSD
    if DDC "$(cat $PMConfig/scheduler.lock.conf)" == "0" ]];then
        local ChangeTo="noop"
        DDC ! -z "${1}" ]] && ChangeTo="${1}"
    else
        local ChangeTo="$(cat $PMConfig/scheduler.conf)"
    fi
    for BlockSD in $(ls /sys/block | grep sd)
    do
        if DDC "${#BlockSD}" == "3" ]];then
            WriteTo "$ChangeTo" "/sys/block/$BlockSD/queue/scheduler"
        fi
    done
    for BlockSD in $(ls /sys/block | grep mmcblk)
    do
        if DDC "${#BlockSD}" == "3" ]];then
            WriteTo "$ChangeTo" "/sys/block/$BlockSD/queue/scheduler"
        fi
    done
    GetErrorMsg "SetScheduler"
} 2>>$RLOGsE

BackupConfig()
{
    DoBackup="n"
    local ListBackup
    for ListBackup in throttling force_no_nap force_bus_on force_clk_on force_rail_on bus_split
    do
        if DDC -f "$NyariGPU/${ListBackup}" ]]; then
            WriteOnly "$(cat $NyariGPU/${ListBackup})" "$PMConfig/backup/gpu_${ListBackup}"
            DoBackup="y"
        fi
    done
    DDC ! -f $PMConfig/backup/sdx_scheduler ]] && DDC -f /sys/block/sda/queue/scheduler ]] && BackupScheduler && DoBackup="y"
    DDC ! -f $PMConfig/backup/mmcblkx_scheduler ]] && DDC -f /sys/block/mmcblk0/queue/scheduler ]] && BackupScheduler && DoBackup="y"

    if DDC -f /proc/sys/kernel/sched_lib_name ]];then
        if DDC ! -z "$(cat /proc/sys/kernel/sched_lib_name)" ]];then
            WriteOnly "$(cat /proc/sys/kernel/sched_lib_name)" "$PMConfig/backup/sched_lib_name" 
            DoBackup="y"
            if DDC -z "$(cat $PMConfig/backup/sched_lib_name)" ]];then
                WriteOnly "UnityMain,libunity.so" "$PMConfig/backup/sched_lib_name"
            fi
        else
            WriteOnly "UnityMain,libunity.so" "$PMConfig/backup/sched_lib_name"
        fi
    fi
    if DDC -f /proc/sys/kernel/sched_lib_mask_force ]];then
        if DDC ! -z "$(cat /proc/sys/kernel/sched_lib_mask_force)" ]];then
            WriteOnly "$(cat /proc/sys/kernel/sched_lib_mask_force)" "$PMConfig/backup/sched_lib_mask_force" 
            DoBackup="y"
            if DDC "$(cat $PMConfig/backup/sched_lib_mask_force)" == "0" ]];then
                WriteOnly "255" "$PMConfig/backup/sched_lib_mask_force"
            fi
        else
            WriteOnly "255" "$PMConfig/backup/sched_lib_mask_force"
        fi
    fi
    DDC "$DoBackup" == "y" ]] && SendLogs "backup config done . . ."
    GetErrorMsg "BackupConfig"
} 2>>$RLOGsE

SetOn()
{
    local ListTweaked
    local NoLogs="n"
    DDC ! -z "${1}" ]] && DDC "${1}" == "silent" ]] && NoLogs="y"
    if DSC -f $NyariGPU/devfreq/adrenoboost ]; then
        WriteOnly "4" "$NyariGPU/devfreq/adrenoboost"
        if DSC "$(cat $NyariGPU/devfreq/adrenoboost)" != "4" ]; then
            WriteOnly "3" "$NyariGPU/devfreq/adrenoboost"
        fi
    fi
    for ListTweaked in throttling force_no_nap force_bus_on force_clk_on force_rail_on
    do
        if DSC -f "$NyariGPU/$ListTweaked" ]; then
            WriteOnly "0" "$NyariGPU/$ListTweaked"
        fi
    done
    if DSC -f "$NyariGPU/bus_split" ]; then
        WriteOnly "1" "$NyariGPU/bus_split"
    fi
    SetScheduler

    if DDC -f /sys/class/thermal/thermal_message/sconfig ]] && DDC "$(cat /sys/class/thermal/thermal_message/sconfig)" != "$(cat $PMConfig/sconfig.thermal.conf)" ]];then
        WriteLockTo "$(cat $PMConfig/sconfig.thermal.conf)" /sys/class/thermal/thermal_message/sconfig
        MemeuiThermalKiller "$(cat $PMConfig/sconfig.thermal.conf)"
    fi

    ## MTK TWEAK ?
    Setppm enabled 1
    Setppm policy_status "1 0"
    Setppm policy_status "2 1"
    Setppm policy_status "3 0"
    Setppm policy_status "4 0"
    Setppm policy_status "5 0"
    Setppm policy_status "7 1"
    Setppm policy_status "9 1"

    SetGedParam gx_3D_benchmark_on "1"
    SetGedParam gx_force_cpu_boost "1"
    SetGedParam gx_game_mode "1"
    
    DDC "$(cat $PMConfig/sconfig.thermal.conf)" == "-1" ]] && SetCpuAllOnline

    SetGpuLimit 0

    if DDC "$NoLogs" == "n" ]];then
        getAppName=""
        local GetAllApps
        for GetAllApps in $(cat $PMConfig/game_list.conf)
        do
            if DDC "${GetAllApps}" == *"---->>"* ]] || DDC "${GetAllApps}" == *"<<----"* ]] || DDC "${GetAllApps}" == *"List-game-installed-start"* ]] || DDC "${GetAllApps}" == *"List-game-installed-end"* ]];then
                getAppName="${getAppName}"
            else
                if DDC ! -z "${getAppName}" ]];then
                    getAppName="${getAppName},"
                fi
                getAppName="${getAppName}${GetAllApps}"
            fi
        done
        if DDC "$getAppName" != *"$AppName"* ]] && DDC "$ModuleMode" != "Force On" ]];then
            if DDC ! -z "${getAppName}" ]];then
                getAppName="${getAppName},"
            fi
            getAppName="${getAppName}${AppName}"
        fi
        if DDC -f "/proc/sys/kernel/sched_lib_name" ]];then
            WriteOnly "$getAppName,UnityMain,libunity.so" "/proc/sys/kernel/sched_lib_name"
        fi
        if DDC -f "/proc/sys/kernel/sched_lib_mask_force" ]];then
            WriteOnly "255" "/proc/sys/kernel/sched_lib_mask_force"
        fi
    fi

    if DDC -f /sys/devices/system/cpu/sched/sched_boost ]];then
        local GetBefore="$(cat /sys/devices/system/cpu/sched/sched_boost)"
        WriteOnly "2" /sys/devices/system/cpu/sched/sched_boost
        DDC "$(cat /sys/devices/system/cpu/sched/sched_boost)" == "$GetBefore" ]] && WriteOnly "1" /sys/devices/system/cpu/sched/sched_boost
    fi

    DDC -f /proc/sys/kernel/sched_boost ]] && WriteOnly "1" /proc/sys/kernel/sched_boost

    DDC "$NoLogs" == "n" ]] && SendLogs "Module Tweak: Enabled"
    DDC "$NoLogs" == "n" ]] && SendLogs "When Use App: $AppName"
    GheymingMode="y"
    GetErrorMsg "SetOn"
} 2>>$RLOGsE

SetOff()
{
    local ListTweaked
    local NoLogs="n"
    DDC ! -z "${1}" ]] && DDC "${1}" == "silent" ]] && NoLogs="y"
    if DSC -f $NyariGPU/devfreq/adrenoboost ]; then
        WriteOnly "0" $NyariGPU/devfreq/adrenoboost
    fi
    for ListTweaked in throttling force_no_nap force_bus_on force_clk_on force_rail_on bus_split
    do
        if DSC -f "$NyariGPU/$ListTweaked" ] && DSC -f "$PMConfig/backup/$ListTweaked" ]; then
            WriteOnly $(cat $PMConfig/backup/gpu_$ListTweaked) "$NyariGPU/$ListTweaked"
        fi
    done
    RestoreScheduler

    if DDC -f /sys/class/thermal/thermal_message/sconfig ]];then
        if DDC "$(cat $PMConfig/sconfig.thermal.lock.conf)" == "1" ]];then
            WriteLockTo "$($PMConfig/sconfig.thermal.conf)" /sys/class/thermal/thermal_message/sconfig
            MemeuiThermalKiller "$(cat $PMConfig/sconfig.thermal.conf)"
        else
            WriteTo "0" /sys/class/thermal/thermal_message/sconfig
            MemeuiThermalKiller "0"
        fi
    fi

    ## MTK TWEAK ?
    Setppm enabled 1
    Setppm policy_status "1 0"
	Setppm policy_status "2 0"
	Setppm policy_status "3 0"
	Setppm policy_status "4 1"
	Setppm policy_status "5 0"
	Setppm policy_status "7 1"
	Setppm policy_status "9 0"

    SetGedParam gx_3D_benchmark_on "0"
    SetGedParam gx_force_cpu_boost "0"
    SetGedParam gx_game_mode "0"

    SetGpuLimit 1

    if DDC -f "/proc/sys/kernel/sched_lib_name" ]];then
        WriteOnly "$(cat $PMConfig/backup/sched_lib_name)" "/proc/sys/kernel/sched_lib_name"
    fi
    if DDC -f "/proc/sys/kernel/sched_lib_mask_force" ]];then
        WriteOnly "$(cat $PMConfig/backup/sched_lib_mask_force)" "/proc/sys/kernel/sched_lib_mask_force"
    fi

    DDC -f /sys/devices/system/cpu/sched/sched_boost ]] && WriteOnly "0" /sys/devices/system/cpu/sched/sched_boost

    DDC -f /proc/sys/kernel/sched_boost ]] && WriteOnly "0" /proc/sys/kernel/sched_boost

    DDC "$NoLogs" == "n" ]] && SendLogs "Module Tweak: Disabled"
    GheymingMode="n"
    GetErrorMsg "SetOff"
} 2>>$RLOGsE

SetSELINUX()
{
    if DDC "${1}" == "change" ]];then
        if DSC "$(getenforce)" == "Enforcing" ]; then
            changeSE="ya"
            setenforce 0
        fi
    fi
    if DDC "${1}" == "restore" ]];then
        if DDC ! -z "$changeSE" ]] && DDC "$changeSE" == "ya" ]];then
            changeSE=""
            setenforce 1
        fi
    fi
} 2>>$RLOGsE

GetStatusGpu()
{
    if DSC -f $NyariGPU/gpu_busy_percentage ]; then
        GetGpuStatus=$(cat "$NyariGPU/gpu_busy_percentage")
    else
        GetGpuStatus="0"
    fi
    if DSC -f $NyariGPU/mem_pool_max_size ] && DSC -f $NyariGPU/mem_pool_size ] && DSC "$MALIGPU" == "YES" ]; then
        MemPollSizeMax=$(cat $NyariGPU/mem_pool_max_size)
        MemPollSize=$(cat $NyariGPU/mem_pool_size)
        GetGpuStatus="$(awk "BEGIN {print (100/$MemPollSizeMax)*$MemPollSize}" | awk -F "\\." '{print $1}')"
    fi
    GpuStatus="$( echo $GetGpuStatus | awk -F'%' '{sub(/^te/,"",$1); print $1 }' )"
    GpuStatus="${GpuStatus/" "/""}"
    GetErrorMsg "GetStatusGpu"
} 2>>$RLOGsE

GetActiveAppName()
{
    AppName="$(dumpsys activity recents | grep 'Recent #0' | awk -F 'A=' '{ print $2 }' | awk -F ' ' '{ print $1 }')"
    if DDC "$AppName" == *":"* ]] ;then
        AppName="$(echo "$AppName" | awk -F ':' '{ print $2 }')"
    fi
    if DDC "$AppName" == *"apex "* ]] ;then
        AppName=""
    fi
    GetErrorMsg "GetActiveAppName"

} 2>>$RLOGsE

CheckCurrentApp()
{
    DDC ! -f $PMConfig/game_list.conf ]] && RegenGameList
    local GameList="$(cat $PMConfig/game_list.conf)"
    local GetListGame
    GameDetected="n"
    if DDC "$GameList" == *"---->> List-game-installed-start <<----"* ]];then
        if DDC "$GameList" == *"$AppName"* ]];then
            GameDetected="y"
        fi
    else
        for GetListGame in $(cat $PMConfig/game_list.conf)
        do
            if DDC "$AppName" == *"$GetListGame"* ]];then
                GameDetected="y"
            fi
        done
    fi
    GetErrorMsg "CheckCurrentApp"
} 2>>$RLOGsE

RegenGameList()
{
    DDC ! -f $PMConfig/manual_game_list.conf ]] && CheckFileConfig
    local GameList=$PMConfig/game_list.conf
    local ListGame
    local GetGameId
    echo "---->> List-game-installed-start <<----" > $GameList
    echo "<<---- List-game-installed-end ---->>" >> $GameList
    for ListGame in $(cat $PMConfig/manual_game_list.conf)
    do
        for GetGameId in $(pm list packages -3 | grep "${ListGame}" | awk -F= '{sub("package:","");print $1}')
        do
            if DDC "$( cat $GameList )" == *"$GetGameId"* ]]; then
                sed -i "1a  $GetGameId" $GameList
            fi
        done
    done
    if DDC "$(wc -l <$PMConfig/game_list.conf)" == "2" ]];then
        cp -af $PMConfig/manual_game_list.conf $PMConfig/game_list.conf
    fi
    GetErrorMsg "RegenGameList"
} 2>>$RLOGsE

SetGedParam()
{
    DDC -d /sys/module/ged/parameters ]] && DDC -f /sys/module/ged/parameters/${1} ]] && WriteOnly "${2}" /sys/module/ged/parameters/${1}
    GetErrorMsg "SetGedParam"
} 2>>$RLOGsE

Setppm()
{
    # [0] PPM_POLICY_PTPOD: enabled
    # [1] PPM_POLICY_UT: enabled
    # [3] PPM_POLICY_FORCE_LIMIT: enabled
    # [2] PPM_POLICY_SYS_BOOST: enabled
    # [4] PPM_POLICY_PWR_THRO: enabled
    # [5] PPM_POLICY_THERMAL: enabled
    # [7] PPM_POLICY_HARD_USER_LIMIT: enabled
    # [8] PPM_POLICY_USER_LIMIT: enabled
    # [9] PPM_POLICY_LCM_OFF: disabled
    # Usage: echo <idx> <1/0> > /proc/ppm/policy_status
    DDC -d /proc/ppm ]] && DDC -f /proc/ppm/${1} ]] && WriteOnly "${2}" /proc/ppm/${1}
    GetErrorMsg "Setppm"
} 2>>$RLOGsE

SetCpuAllOnline()
{
    local i
    for i in 0 1 2 3 4 5 6 7
    do
        DDC -f /sys/devices/system/cpu/cpu$i/online ]] && WriteTo 1 /sys/devices/system/cpu/cpu$i/online
    done
    GetErrorMsg "SetCpuAllOnline"
} 2>>$RLOGsE

SetGpuLimit() {
    # echo [id][up_enable][low_enable] > /proc/gpufreq/gpufreq_limit_table
    # ex: echo 3 0 0 > /proc/gpufreq/gpufreq_limit_table
    # means disable THERMAL upper_limit_idx & lower_limit_idx
    #
    #        [name]  [id]     [prio]   [up_idx] [up_enable]  [low_idx] [low_enable]
    #        STRESS     0          8         -1          0         -1          0
    #          PROC     1          7         34          0         34          0
    #         PTPOD     2          6         -1          0         -1          0
    #       THERMAL     3          5         -1          0         -1          0
    #       BATT_OC     4          5         -1          0         -1          0
    #      BATT_LOW     5          5         -1          0         -1          0
    #  BATT_PERCENT     6          5         -1          0         -1          0
    #           PBM     7          5         -1          0         -1          0
    #        POLICY     8          4         -1          0         -1          0
    if DDC -f /proc/gpufreq/gpufreq_limit_table ]];then
        limited="${1}"
        local i
        for i in 0 1 2 3 4 5 6 7 8
        do
            # echo $i $limited $limited
            WriteOnly "$i $limited $limited" "/proc/gpufreq/gpufreq_limit_table"
        done
    fi
    GetErrorMsg "SetGpuLimit"
} 2>>$RLOGsE

UpdateFastCharging()
{
    if DDC -f /sys/kernel/fast_charge/force_fast_charge ]];then
        SendLogs "Kernel Supported Fastcharging, enabling it"
        WriteOnly 2 /sys/kernel/fast_charge/force_fast_charge
        DDC "$(cat /sys/kernel/fast_charge/force_fast_charge)" == "0" ]] && WriteOnly 1 /sys/kernel/fast_charge/force_fast_charge
    fi
    GetErrorMsg "UpdateFastCharging"
} 2>>$RLOGsE

DoFstrim(){
    GetFstrim="$(cat $MPATH/system/etc/ZyC-Core/info/last_fstrim.log)"
    if DDC "$GetFstrim" != "$( date +"%Y-%m-%d" )" ]];then
        if DDC "$GetBusyBox" != "none" ]];then
            fstrim -v /cache 1>>$LOGs
            fstrim -v /data 1>>$LOGs
            fstrim -v /system 1>>$LOGs
            SendLogs "fstrim data cache & system done . . ."
        else
            SendLogs "Cannot do fstrim, because busybox not installed"
        fi
        WriteOnly "$( date +"%Y-%m-%d" )" $MPATH/system/etc/ZyC-Core/info/last_fstrim.log
    fi
    GetErrorMsg "DoFstrim"
} 2>>$RLOGsE

DoSqlite(){
    GetSqLite="$(cat $MPATH/system/etc/ZyC-Core/info/last_optimize_database.log)"
    if DDC "$GetSqLite" != "$( date +"%Y-%m-%d" )" ]];then
        if DDC "$GetBusyBox" != "none" ]];then
            local FilePath=""
            local ThrowInfo=""
            echo "$( date +"%H:%M:%S") | Start" > $MPATH/system/etc/ZyC-Core/info/optimize_database.log
            if DDC -d /data ]];then
                DDC -f /system/xbin/sqlite3 ]] && local SqLitePath=/system/xbin/sqlite3
                DDC -f /system/bin/sqlite3 ]] && local SqLitePath=/system/bin/sqlite3
                for FilePath in $(find /data -iname "*.db")
                do
                    ThrowInfo="n"
                    $SqLitePath $FilePath 'VACUUM;' 2>>/dev/null && ThrowInfo="y"
                    $SqLitePath $FilePath 'REINDEX;' 2>>/dev/null && ThrowInfo="y"
                    DDC "$ThrowInfo" == "y" ]] && echo "optimizing $FilePath" >> $MPATH/system/etc/ZyC-Core/info/optimize_database.log
                done
            fi
            echo "$( date +"%H:%M:%S") | End" >> $MPATH/system/etc/ZyC-Core/info/optimize_database.log
            SendLogs "optimze database done . . ."
        else
            echo "$( date +"%H:%M:%S") | Need Busybox for this things" > $MPATH/system/etc/ZyC-Core/info/optimize_database.log
            SendLogs "Cannot optimze database, because busybox not installed"
        fi
        WriteOnly "$( date +"%Y-%m-%d" )" $MPATH/system/etc/ZyC-Core/info/last_optimize_database.log
    fi
    GetErrorMsg "DoSqlite"
} 2>>$RLOGsE

MTKSwitchCpuMode()
{
    if DDC ! -z "${1}" ]];then
        local CpuModePath=/sys/devices/system/cpu/eas/enable
        local GetMode="$(MtkCpuThings "${1}" number)"
        local GetModeDetail="$(MtkCpuThings "${1}" string)"
        if DDC -f ${CpuModePath} ]] && DDC "$GetMode" != *"Not Supported"* ]];then
            local GetBefore="$(MtkCpuThings "$CpuModePath" string)"
            if DDC "$GetBefore" != "$GetModeDetail" ]];then
                WriteOnly "$GetMode" "${CpuModePath}"
                sleep 1s
                if DDC "$(MtkCpuThings "$CpuModePath" string)" != "$GetBefore" ]];then
                    SendLogs "Switch kernel mode to $GetModeDetail done"
                else
                    SendLogs "Switch kernel mode failed($CpuChangeFail), ur current kernel not supported it"
                    if DDC "${CpuChangeFail}" -lt "5" ]];then
                        CpuChangeFail=$(($CpuChangeFail+1))
                    else
                        DDC "$(cat $PMConfig/mtk_cpu_mode_off.conf)" != "Not Supported" ]] && WriteOnly "Not Supported" $PMConfig/mtk_cpu_mode_off.conf
                        DDC "$(cat $PMConfig/mtk_cpu_mode_on.conf)" != "Not Supported" ]] && WriteOnly "Not Supported" $PMConfig/mtk_cpu_mode_on.conf
                    fi
                fi
            fi
        fi
    else
        DDC "$(cat $PMConfig/mtk_cpu_mode_off.conf)" != "Not Supported" ]] && WriteOnly "Not Supported" $PMConfig/mtk_cpu_mode_off.conf
        DDC "$(cat $PMConfig/mtk_cpu_mode_on.conf)" != "Not Supported" ]] && WriteOnly "Not Supported" $PMConfig/mtk_cpu_mode_on.conf
    fi
    GetErrorMsg "SwitchCpuMode"
} 2>>$RLOGsE

MtkCpuThings()
{
    if DDC ! -z "$1" ]] && DDC ! -z "$2" ]];then
        local GetMode="$(cat $1)"
        if DDC "${GetMode}" == "<auto-generated>" ]] || DDC -f "${1}" ]];then
            CheckFileConfig && GetMode=$(cat ${1})
        fi
        local output=""
        if DDC "$GetMode" != "Not Supported" ]];then
            if DDC "$(echo $GetMode | tr '[:lower:]' '[:upper:]' )" == *"HMP"* ]] || DDC "$GetMode" == "0" ]];then
                DDC "$2" == "string" ]] && output="HMP"
                DDC "$2" == "number" ]] && output="0"
            elif DDC "$(echo $GetMode | tr '[:lower:]' '[:upper:]' )" == *"EAS"* ]] || DDC "$GetMode" == "1" ]];then
                DDC "$2" == "string" ]] && output="EAS"
                DDC "$2" == "number" ]] && output="1"
            elif DDC "$(echo $GetMode | tr '[:lower:]' '[:upper:]' )" == *"HYBRID"* ]] || DDC "$GetMode" == "2" ]];then
                DDC "$2" == "string" ]] && output="HYBRID"
                DDC "$2" == "number" ]] && output="2"
            else
                output="Not Supported"
            fi
        else
            output="Not Supported"
        fi
        DDC -z "$output" ]] && output="Not Supported"
        echo "$output"
    fi
}

DoDropCache()
{
    local DropVal="$(cat $PMConfig/drop_caches.conf)"
    local DropValTime="$(cat $PMConfig/drop_caches_time.conf)"
    local DoDropCacheNow=""
    local clearType=""
    DDC "$(GetDisplayStatus)" == "off" ]] && DDC "$DropValTime" -le "20" ]] && DropValTime="$(($DropValTime*2))"
    if DDC "$(date +"%H")" == "23" ]];then
        DropVal="0"
        DDC ! -z "$LastDropCache" ]] && LastDropCache=""
    else
        if DDC -f /proc/sys/vm/drop_caches ]] && DDC "$DropVal" != "0" ]];then
            if DDC -z "$LastDropCache" ]];then
                DoDropCacheNow="y"
            elif DDC "$LastDropCache" -lt "$(AddTime "0")" ]];then
                DoDropCacheNow="y"
            else
                DoDropCacheNow="n"
            fi
            if DDC "$DropVal" -gt "3" ]];then
                DropVal="3"
                WriteOnly $DropVal $PMConfig/drop_caches.conf
            elif DDC "$DropVal" -lt "0" ]];then
                DropVal="0"
                WriteOnly $DropVal $PMConfig/drop_caches.conf
                DoDropCacheNow="n"
            fi
            if DDC "$DoDropCacheNow" == "y" ]];then
                # 1 = clear pageCache only.
                # 2 = clear dentries and inodes.
                # 3 = clear pagecache, dentries, and inodes.
                DDC "$DropVal" == "1" ]] && clearType="pageCache"
                DDC "$DropVal" == "2" ]] && clearType="dentries and inodes"
                DDC "$DropVal" == "3" ]] && clearType="pagecache, dentries, and inodes"
                WriteOnly $DropVal /proc/sys/vm/drop_caches
                LastDropCache="$(AddTime "$DropValTime")"
                SendLogs "clear ram $clearType success, will be triggered again at $(AddTime "$DropValTime" proper)"
            fi
        fi
    fi
    GetErrorMsg "DoDropCache"
} 2>>$RLOGsE

SetForceDoze()
{
    DDC ! -f $PMConfig/force_doze.conf ]] && CheckFileConfig
    local ForceDozeConfig="$(cat $PMConfig/force_doze.conf)"
    if DDC "$ForceDozeConfig" == "1" ]];then
        local DisplayStatus="$(GetDisplayStatus)"
        if DDC ! -z "$DisplayStatus" ]] && DDC "$DisplayStatus" != "unknow" ]];then
            if DDC "$ScreenState" == "on" ]] && DDC "$DisplayStatus" == "off" ]];then
                if DDC "$SwitchForceDoze" == "2" ]];then
                    dumpsys deviceidle force-idle
                    ScreenState="off"
                    SendLogs "Screen off,forcing to doze state"
                    SwitchForceDoze="0"
                    CpuFreqLock "Sleep"
                else
                    SwitchForceDoze=$(($SwitchForceDoze+1))
                fi
                DDC "$GheymingMode" == "y" ]] && SetOff "silent"
            elif DDC "$ScreenState" == "off" ]] && DDC "$DisplayStatus" == "on" ]];then
                dumpsys deviceidle unforce
                ScreenState="on"
                SendLogs "Screen on,unforcing doze state"
                CpuFreqLock "Unsleep"
            fi
        fi
    else
        if DDC "$ForceDozeConfig" != "0" ]];then
            WriteOnly "0" $PMConfig/force_doze.conf
        fi
    fi
    GetErrorMsg "SetForceDoze"
} 2>>$RLOGsE

CheckFileConfig()
{
    DDC ! -f $PMConfig/min_gpu_usage.conf ]] && WriteOnly "5" $PMConfig/min_gpu_usage.conf

    DDC ! -f $PMConfig/max_gpu_usage.conf ]] && WriteOnly "70" $PMConfig/max_gpu_usage.conf

    DDC ! -f $PMConfig/max_check_gpu_usage.conf ]] && WriteOnly "3" $PMConfig/max_check_gpu_usage.conf

    DDC ! -f $PMConfig/wait_when_on.conf ]] && WriteOnly "10s" $PMConfig/wait_when_on.conf

    DDC ! -f $PMConfig/wait_when_off.conf ]] && WriteOnly "5s" $PMConfig/wait_when_off.conf

    DDC ! -f $PMConfig/manual_game_list.conf ]] && WriteOnly "com.mobile.legends com.pubg.krmobile com.pwrd.pwm tw.com.szn.lz com.zloong.eu.dr.gp com.archosaur.sea.dr.gp com.kr.krlz.google com.tencent com.garena com.miHoYo com.gamedreamer com.netease" $PMConfig/manual_game_list.conf

    DDC ! -f $PMConfig/modules_mode.conf ]] && WriteOnly "Auto" $PMConfig/modules_mode.conf

    if DDC -f /sys/class/thermal/thermal_message/sconfig ]];then

        DDC ! -f $PMConfig/sconfig.thermal.conf ]] && WriteOnly "16" $PMConfig/sconfig.thermal.conf

        DDC ! -f $PMConfig/sconfig.thermal.lock.conf ]] && WriteOnly "0" $PMConfig/sconfig.thermal.lock.conf
    else
        if DDC -f $PMConfig/sconfig.thermal.conf ]] ;then
            DDC "$(cat $PMConfig/sconfig.thermal.conf)" != "Not Supported" ]] && WriteOnly "Not Supported" $PMConfig/sconfig.thermal.conf
        else
            WriteOnly "Not Supported" $PMConfig/sconfig.thermal.conf
        fi
        
        if DDC -f $PMConfig/sconfig.thermal.lock.conf ]];then
            DDC "$(cat $PMConfig/sconfig.thermal.lock.conf)" != "Not Supported" ]] && WriteOnly "Not Supported" $PMConfig/sconfig.thermal.lock.conf
        else
            WriteOnly "Not Supported" $PMConfig/sconfig.thermal.lock.conf
        fi

    fi

    if DDC ! -f $PMConfig/scheduler.conf ]] || DDC "$(cat $PMConfig/scheduler.conf)" == "default" ]];then
        local ChangeSchedTo="cfq"
        local BlockSD
        for BlockSD in $(ls /sys/block | grep sd)
        do
            if DDC "${#BlockSD}" == "3" ]];then
                local SchedList="$(cat /sys/block/$BlockSD/queue/scheduler)"
                if DDC "$SchedList" == *"noop"* ]];then
                    ChangeSchedTo="noop" && break
                fi
            fi
        done
        if DDC "$ChangeSchedTo" == "cfq" ]];then
            for BlockSD in $(ls /sys/block | grep mmcblk)
            do
                if DDC "${#BlockSD}" == "7" ]];then
                    local SchedList="$(cat /sys/block/$BlockSD/queue/scheduler)"
                    if DDC "$SchedList" == *"noop"* ]];then
                        ChangeSchedTo="noop" && break
                    fi
                fi
            done
        fi
        WriteOnly $ChangeSchedTo $PMConfig/scheduler.conf
    fi

    DDC ! -f $PMConfig/scheduler.lock.conf ]] && WriteOnly "0" $PMConfig/scheduler.lock.conf

    DDC ! -f $PMConfig/show_error.conf ]] && WriteOnly "0" $PMConfig/show_error.conf

    if DDC -f /sys/devices/system/cpu/eas/enable ]];then
        if DDC ! -f $PMConfig/mtk_cpu_mode_on.conf ]];then
            WriteOnly "0" $PMConfig/mtk_cpu_mode_on.conf
        else
            DDC "$(cat $PMConfig/mtk_cpu_mode_on.conf)" == "<auto-generated>" ]] && WriteOnly "0" $PMConfig/mtk_cpu_mode_on.conf
        fi
        if DDC ! -f $PMConfig/mtk_cpu_mode_off.conf ]];then
            WriteOnly "1" $PMConfig/mtk_cpu_mode_off.conf
        else
            DDC "$(cat $PMConfig/mtk_cpu_mode_off.conf)" == "<auto-generated>" ]] && WriteOnly "1" $PMConfig/mtk_cpu_mode_off.conf
        fi
    else
        if DDC ! -f $PMConfig/mtk_cpu_mode_off.conf ]];then
            WriteOnly "Not Supported" $PMConfig/mtk_cpu_mode_off.conf
        else
            DDC "$(cat $PMConfig/mtk_cpu_mode_off.conf)" != "Not Supported" ]] && WriteOnly "Not Supported" $PMConfig/mtk_cpu_mode_off.conf
        fi
        if DDC ! -f $PMConfig/mtk_cpu_mode_on.conf ]];then
            WriteOnly "Not Supported" $PMConfig/mtk_cpu_mode_on.conf
        else
            DDC "$(cat $PMConfig/mtk_cpu_mode_on.conf)" != "Not Supported" ]] && WriteOnly "Not Supported" $PMConfig/mtk_cpu_mode_on.conf
        fi
    fi

    if DDC -f /proc/sys/vm/drop_caches ]];then
        DDC ! -f $PMConfig/drop_caches.conf ]] && WriteOnly "0" $PMConfig/drop_caches.conf
        DDC ! -f $PMConfig/drop_caches_time.conf ]] && WriteOnly "60" $PMConfig/drop_caches_time.conf
    else
        if DDC -f $PMConfig/drop_caches.conf ]];then
            DDC "$(cat $PMConfig/drop_caches.conf)" != "Not Supported" ]] && WriteOnly "Not Supported" $PMConfig/drop_caches.conf
        else
            WriteOnly "Not Supported" $PMConfig/drop_caches.conf
        fi
        if DDC -f $PMConfig/drop_caches_time.conf ]];then
            DDC "$(cat $PMConfig/drop_caches_time.conf)" != "Not Supported" ]] && WriteOnly "Not Supported" $PMConfig/drop_caches_time.conf
        else
            WriteOnly "Not Supported" $PMConfig/drop_caches_time.conf
        fi
    fi

    DDC ! -f $PMConfig/force_doze.conf ]] && WriteOnly "0" $PMConfig/force_doze.conf

    if DDC -f /sys/module/proc/parameters/sultan_pid_mode ]] || DDC -f /sys/module/lyb_taskmmu/parameters/lyb_sultan_pid_shrink ]] || DDC -f /sys/module/lyb_taskmmu/parameters/lyb_sultan_pid ]];then
         DDC ! -f $PMConfig/sultan_pid_app.conf ]] && WriteOnly "com.miHoYo.GenshinImpact" $PMConfig/sultan_pid_app.conf
    else
        if DDC -f $PMConfig/sultan_pid_app.conf ]];then
            DDC "$(cat $PMConfig/sultan_pid_app.conf)" != "Not Supported" ]] && WriteOnly "Not Supported" $PMConfig/sultan_pid_app.conf
        else
            WriteOnly "Not Supported" $PMConfig/sultan_pid_app.conf
        fi
    fi

    DDC ! -f $PMConfig/use_cpu_tweak.conf ]] && WriteOnly "1" $PMConfig/use_cpu_tweak.conf

    DDC ! -f $PMConfig/silent_overwrite.conf ]] && WriteOnly "0" $PMConfig/silent_overwrite.conf

    DDC ! -f $PMConfig/write_info.conf ]] && WriteOnly "0" $PMConfig/write_info.conf

    DDC ! -f $PMConfig/status.conf ]] && WriteOnly "1" $PMConfig/status.conf

    GetErrorMsg "CheckFileConfig"
} 2>>$RLOGsE

RealGetDisplayStatus()
{
    local stopNow="n"
    local GetByPowerService="$( dumpsys power 2>>$RLOGsE | grep "mHoldingDisplaySuspendBlocker" | sed 's/mHoldingDisplaySuspendBlocker=*//g' )"
    if DDC "$GetByPowerService" == *"ON"* ]];then
        DDC "$stopNow" == "n" ]] && echo "on"
        stopNow="y"
    elif DDC "$GetByPowerService" == *"OFF"* ]];then
        DDC "$stopNow" == "n" ]] && echo "off"
        stopNow="y"
    fi
    if DDC "$stopNow" == "n" ]];then
        local GetByDisplayService="$( dumpsys display 2>>$RLOGsE | grep "mScreenState" | sed 's/mScreenState=*//g' )"
        if DDC "$GetByDisplayService" == *"ON"* ]];then
            DDC "$stopNow" == "n" ]] && echo "on"
            stopNow="y"
        elif DDC "$GetByDisplayService" == *"OFF"* ]];then
            DDC "$stopNow" == "n" ]] && echo "off"
            stopNow="y"
        fi
    fi
    if DDC "$stopNow" == "n" ]];then
        local GetByNFCService="$( dumpsys nfc 2>>$RLOGsE | grep 'mScreenState=' | sed 's/mScreenState=*//g' )"
        if DDC "$GetByNFCService" == *"ON_LOCKED"* ]] && DDC "$GetByNFCService" == *"ON_UNLOCKED"* ]];then
            DDC "$stopNow" == "n" ]] && echo "on"
            stopNow="y"
        elif DDC "$GetByNFCService" == *"OFF_LOCKED"* ]] && DDC "$GetByNFCService" == *"OFF_UNLOCKED"* ]];then
            DDC "$stopNow" == "n" ]] && echo "off"
            stopNow="y"
        fi
    fi
    DDC "$stopNow" == "n" ]] && echo "unknow"
    GetErrorMsg "RealGetDisplayStatus"
} 2>>$RLOGsE

GetDisplayStatus()
{
    DDC "$StopGetDisplay" == "n" ]] && CurrentDisplayStatus="$(RealGetDisplayStatus)"
    StopGetDisplay="y"
    if DDC -z "$CurrentDisplayStatus" ]];then
        echo "on"
    else
        echo "$CurrentDisplayStatus"
    fi
}

SultanPidAppModeChanger()
{
    local getConfig
    local changemode="n"
    if DDC -f /sys/module/proc/parameters/sultan_pid_mode ]];then
        for getConfig in $(cat $PMConfig/sultan_pid_app.conf)
        do
            if DDC "$AppName" == *"$getConfig"* ]];then
                changemode="y"
                break
            fi
        done
        if DDC "$changemode" == "y" ]];then
            DDC "$(cat /sys/module/proc/parameters/sultan_pid_mode)" != "3" ]] && WriteOnly "3" /sys/module/proc/parameters/sultan_pid_mode
        else
            DDC "$(cat /sys/module/proc/parameters/sultan_pid_mode)" != "0" ]] && WriteOnly "0" /sys/module/proc/parameters/sultan_pid_mode
        fi
    fi
    if DDC -f /sys/module/lyb_taskmmu/parameters/lyb_sultan_pid ]] && DDC -f  -f /sys/module/lyb_taskmmu/parameters/lyb_sultan_pid_shrink ]];then
        for getConfig in $(cat $PMConfig/sultan_pid_app.conf)
        do
            if DDC "$AppName" == *"$getConfig"* ]];then
                changemode="y"
                break
            fi
        done
        if DDC "$changemode" == "y" ]];then
            DDC "$(cat /sys/module/lyb_taskmmu/parameters/lyb_sultan_pid)" != "Y" ]] && WriteOnly "Y" /sys/module/lyb_taskmmu/parameters/lyb_sultan_pid
            DDC "$(cat /sys/module/lyb_taskmmu/parameters/lyb_sultan_pid_shrink)" != "Y" ]] && WriteOnly "Y" /sys/module/lyb_taskmmu/parameters/lyb_sultan_pid_shrink
        else
            DDC "$(cat /sys/module/lyb_taskmmu/parameters/lyb_sultan_pid)" != "N" ]] && WriteOnly "N" /sys/module/lyb_taskmmu/parameters/lyb_sultan_pid
            DDC "$(cat /sys/module/lyb_taskmmu/parameters/lyb_sultan_pid_shrink)" != "N" ]] && WriteOnly "N" /sys/module/lyb_taskmmu/parameters/lyb_sultan_pid_shrink
        fi
    fi
    GetErrorMsg "SultanPidAppModeChanger"
}

CpuBalance()
{
    local UINT_MAX="4294967295"
    local SCHED_PERIOD="$((4 * 1000 * 1000))"
    local SCHED_TASKS="8"
    local IFS
    local governor
    local queue
    WriteSysKernel  perf_cpu_time_max_percent:5 sched_autogroup_enabled:1 sched_child_runs_first:1 sched_tunable_scaling:0 "sched_latency_ns:$SCHED_PERIOD" \
                    "sched_min_granularity_ns:$((SCHED_PERIOD / SCHED_TASKS))" "sched_wakeup_granularity_ns:$((SCHED_PERIOD / 2))" sched_migration_cost_ns:5000000 \
                    sched_min_task_util_for_colocation:0 sched_nr_migrate:32 sched_schedstats:0 printk_devkmsg:off
    WriteOnly 0 /dev/stune/top-app/schedtune.prefer_idle
    WriteOnly 1 /dev/stune/top-app/schedtune.boost

    find /sys/devices/system/cpu/ -name schedutil -type d | while IFS= read -r governor
    do
        WriteOnly "$((SCHED_PERIOD / 1000))" "$governor/up_rate_limit_us"
        WriteOnly "$((4 * SCHED_PERIOD / 1000))" "$governor/down_rate_limit_us" 
        WriteOnly "$((SCHED_PERIOD / 1000))" "$governor/rate_limit_us"
        WriteOnly 90 "$governor/hispeed_load"
        WriteOnly "$UINT_MAX" "$governor/hispeed_freq"
    done

    find /sys/devices/system/cpu/ -name blu_schedutil -type d | while IFS= read -r governor
    do
        WriteOnly "$((SCHED_PERIOD / 1000))" "$governor/up_rate_limit_us"
        WriteOnly "$((4 * SCHED_PERIOD / 1000))" "$governor/down_rate_limit_us" 
        WriteOnly "$((SCHED_PERIOD / 1000))" "$governor/rate_limit_us"
        WriteOnly 90 "$governor/hispeed_load"
        WriteOnly "$UINT_MAX" "$governor/hispeed_freq"
    done

    find /sys/devices/system/cpu/ -name interactive -type d | while IFS= read -r governor
    do
        WriteOnly "$((SCHED_PERIOD / 1000))" "$governor/timer_rate"
        WriteOnly "$((SCHED_PERIOD / 1000))" "$governor/min_sample_time"
        WriteOnly 90 "$governor/go_hispeed_load"
        WriteOnly "$UINT_MAX" "$governor/hispeed_freq"
    done

    for queue in /sys/block/*/queue
    do
        WriteOnly 0 "$queue/add_random"
        WriteOnly 0 "$queue/iostats"
        WriteOnly 128 "$queue/read_ahead_kb"
        WriteOnly 64 "$queue/nr_requests"
    done
}

VmBalance()
{
    WriteSysVm  dirty_background_ratio:10 dirty_ratio:30 dirty_expire_centisecs:3000 dirty_writeback_centisecs:3000 page-cluster:0 stat_interval:10 \
                swappiness:100 vfs_cache_pressure:100
}

CpuPerformance()
{
    local UINT_MAX="4294967295"
    local SCHED_PERIOD="$((10 * 1000 * 1000))"
    local SCHED_TASKS="6"
    local IFS
    local governor
    local queue
    WriteSysKernel  perf_cpu_time_max_percent:20 sched_autogroup_enabled:0 sched_child_runs_first:0 sched_tunable_scaling:0 "sched_latency_ns:$SCHED_PERIOD" \
                    "sched_min_granularity_ns:$((SCHED_PERIOD / SCHED_TASKS))" "sched_wakeup_granularity_ns:$((SCHED_PERIOD / 2))" sched_migration_cost_ns:5000000 \
                    sched_min_task_util_for_colocation:0 sched_nr_migrate:128 sched_schedstats:0 printk_devkmsg:off

    WriteOnly 0 /dev/stune/top-app/schedtune.prefer_idle
    WriteOnly 1 /dev/stune/top-app/schedtune.boost

    find /sys/devices/system/cpu/ -name schedutil -type d | while IFS= read -r governor
    do
        WriteOnly "$((SCHED_PERIOD / 1000))" "$governor/up_rate_limit_us"
        WriteOnly "$((4 * SCHED_PERIOD / 1000))" "$governor/down_rate_limit_us"
        WriteOnly "$((SCHED_PERIOD / 1000))" "$governor/rate_limit_us"
        WriteOnly 85 "$governor/hispeed_load"
        WriteOnly "$UINT_MAX" "$governor/hispeed_freq"
    done

    find /sys/devices/system/cpu/ -name blu_schedutil -type d | while IFS= read -r governor
    do
        WriteOnly "$((SCHED_PERIOD / 1000))" "$governor/up_rate_limit_us"
        WriteOnly "$((4 * SCHED_PERIOD / 1000))" "$governor/down_rate_limit_us" 
        WriteOnly "$((SCHED_PERIOD / 1000))" "$governor/rate_limit_us"
        WriteOnly 85 "$governor/hispeed_load"
        WriteOnly "$UINT_MAX" "$governor/hispeed_freq"
    done

    find /sys/devices/system/cpu/ -name interactive -type d | while IFS= read -r governor
    do
        WriteOnly "$((SCHED_PERIOD / 1000))" "$governor/timer_rate"
        WriteOnly "$((SCHED_PERIOD / 1000))" "$governor/min_sample_time"
        WriteOnly 85 "$governor/go_hispeed_load"
        WriteOnly "$UINT_MAX" "$governor/hispeed_freq"
    done

    for queue in /sys/block/*/queue
    do
        WriteOnly 0 "$queue/add_random"
        WriteOnly 0 "$queue/iostats"
        WriteOnly 256 "$queue/read_ahead_kb"
        WriteOnly 512 "$queue/nr_requests"
    done

}

VmPerformance()
{
    WriteSysVm  dirty_background_ratio:15 dirty_ratio:30 dirty_expire_centisecs:3000 dirty_writeback_centisecs:3000 page-cluster:0 stat_interval:10 \
                swappiness:100 vfs_cache_pressure:80
}

SetFreqCpu()
{
    local Lt="-1"
    local Bg="-1"
    local Pm="-1"
    local mode="$1"
    local ForLt="$2"
    local ForBg="$3"
    local ForPm="$4"
    local TotalClusters="0"
    for GetClustersNumber in $(ls /sys/devices/system/cpu/cpufreq | grep policy)
    do
        TotalClusters=$(($TotalClusters+1))
        if DDC "$Lt" == "-1" ]];then
            Lt="$(echo $GetClustersNumber | awk -F 'policy' '{print $2}')"
            if DDC "$2" == "default" ]] || DDC -z "$ForLt" ]] || DDC "$ForLt" == "0" ]];then
                local GetInfoFreq
                for GetInfoFreq in $(cat /sys/devices/system/cpu/cpufreq/$GetClustersNumber/scaling_available_frequencies)
                do
                    if DDC -z "$ForLt" ]] || DDC "$ForLt" == "0" ]] || DDC -z "$(echo $ForLt | grep -q "^[0-9]*$" && echo "OK")" ]];then
                        ForLt="$GetInfoFreq"
                    elif DDC "$mode" == "max" ]] && DDC "$ForLt" -le "$GetInfoFreq" ]];then
                        ForLt="$GetInfoFreq"
                    elif DDC "$mode" == "min" ]] && DDC "$ForLt" -ge "$GetInfoFreq" ]];then
                        ForLt="$GetInfoFreq"
                    fi
                done
            fi
            WriteOnly "$ForLt" /sys/devices/system/cpu/cpufreq/$GetClustersNumber/scaling_${mode}_freq
            WriteTo "$ForLt" /sys/devices/system/cpu/cpufreq/$GetClustersNumber/cpuinfo_${mode}_freq
        elif DDC "$Bg" == "-1" ]];then
            Bg="$(echo $GetClustersNumber | awk -F 'policy' '{print $2}')"
            if DDC "$2" == "default" ]] || DDC -z "$ForBg" ]] || DDC "$ForBg" == "0" ]];then
                local GetInfoFreq
                for GetInfoFreq in $(cat /sys/devices/system/cpu/cpufreq/$GetClustersNumber/scaling_available_frequencies)
                do
                    if DDC -z "$ForBg" ]] || DDC "$ForBg" == "0" ]] || DDC -z "$(echo $ForBg | grep -q "^[0-9]*$" && echo "OK")" ]];then
                        ForBg="$GetInfoFreq"
                    elif DDC "$mode" == "max" ]] && DDC "$ForBg" -le "$GetInfoFreq" ]];then
                        ForBg="$GetInfoFreq"
                    elif DDC "$mode" == "min" ]] && DDC "$ForBg" -ge "$GetInfoFreq" ]];then
                        ForBg="$GetInfoFreq"
                    fi
                done
            fi
            WriteOnly "$ForBg" /sys/devices/system/cpu/cpufreq/$GetClustersNumber/scaling_${mode}_freq
            WriteTo "$ForBg" /sys/devices/system/cpu/cpufreq/$GetClustersNumber/cpuinfo_${mode}_freq
        elif DDC "$Pm" == "-1" ]];then
            Pm="$(echo $GetClustersNumber | awk -F 'policy' '{print $2}')"
            if DDC "$2" == "default" ]] || DDC -z "$ForPm" ]] || DDC "$ForPm" == "0" ]];then
                local GetInfoFreq
                for GetInfoFreq in $(cat /sys/devices/system/cpu/cpufreq/$GetClustersNumber/scaling_available_frequencies)
                do
                    if DDC -z "$ForPm" ]] || DDC "$ForPm" == "0" ]] || DDC -z "$(echo $ForPm | grep -q "^[0-9]*$" && echo "OK")" ]];then
                        ForPm="$GetInfoFreq"
                    elif DDC "$mode" == "max" ]] && DDC "$ForPm" -le "$GetInfoFreq" ]];then
                        ForPm="$GetInfoFreq"
                    elif DDC "$mode" == "min" ]] && DDC "$ForPm" -ge "$GetInfoFreq" ]];then
                        ForPm="$GetInfoFreq"
                    fi
                done
            fi
            WriteOnly "$ForPm" /sys/devices/system/cpu/cpufreq/$GetClustersNumber/scaling_${mode}_freq
            WriteTo "$ForPm" /sys/devices/system/cpu/cpufreq/$GetClustersNumber/cpuinfo_${mode}_freq
        fi
    done
    local prepCmd=""
    local ListCPUs
    local SpaceCharacter=" "
    for ListCPUs in 0 1 2 3 4 5 6 7;do
        [[ "$ListCPUs" == "7" ]] && SpaceCharacter=""
        if DDC "$ListCPUs" -ge "$Pm" ]];then
            prepCmd="${prepCmd}${ListCPUs}:${ForPm}${SpaceCharacter}"
        elif DDC "$ListCPUs" -ge "$Bg" ]];then
            prepCmd="${prepCmd}${ListCPUs}:${ForBg}${SpaceCharacter}"
        elif DDC "$ListCPUs" -ge "$Lt" ]];then
            prepCmd="${prepCmd}${ListCPUs}:${ForLt}${SpaceCharacter}"
        fi
    done
    if DDC -e /sys/module/msm_performance/parameters/cpu_${mode}_freq ]];then
        WriteOnly "$prepCmd" /sys/module/msm_performance/parameters/cpu_${mode}_freq
    fi
    if DDC -e "/proc/ppm/policy/hard_userlimit_${mode}_cpu_freq" ]];then
        Setppm enabled 1
        Setppm policy_status "7 1"
        DDC "$TotalClusters" -ge "1" ]] && WriteOnly "0 $ForLt" "/proc/ppm/policy/hard_userlimit_${mode}_cpu_freq" && SendLogs "Set $mode Little Cluster To $1"
        DDC "$TotalClusters" -ge "2" ]] && WriteOnly "1 $ForBg" "/proc/ppm/policy/hard_userlimit_${mode}_cpu_freq" && SendLogs "Set $mode Big Cluster To $2"
        DDC "$TotalClusters" -ge "3" ]] && WriteOnly "2 $ForPm" "/proc/ppm/policy/hard_userlimit_${mode}_cpu_freq" && SendLogs "Set $mode Prime Cluster To $3"
    fi
    # ls  /sys/module/cpu_boost/parameters/
    # input_boost_freq  input_boost_ms  sched_boost_on_input
    if DDC -d /sys/module/cpu_boost/parameters ]];then
        if DDC "$GheymingMode" == "y" ]];then
            if DDC "$mode" == "max" ]];then
                WriteOnly "$prepCmd" /sys/module/cpu_boost/parameters/input_boost_freq
                WriteOnly "120" /sys/module/cpu_boost/parameters/input_boost_ms
            fi
        else
            if DDC "$mode" == "min" ]];then
                WriteOnly "$prepCmd" /sys/module/cpu_boost/parameters/input_boost_freq
                WriteOnly "0" /sys/module/cpu_boost/parameters/input_boost_ms
            fi
        fi
    fi
    GetErrorMsg "SetFreqCpu"
} 2>>$RLOGsE

CpuSetOn()
{
    local i
    for i in 0 1 2 3 4 5 6 7
    do
        [[ ! -z "$(cat $1 | grep $i)" ]] && DDC -f /sys/devices/system/cpu/cpu$i/online ]] && WriteTo 1 /sys/devices/system/cpu/cpu$i/online
        [[ -z "$(cat $1 | grep $i)" ]] && DDC -f /sys/devices/system/cpu/cpu$i/online ]] && WriteTo 0 /sys/devices/system/cpu/cpu$i/online
    done
    GetErrorMsg "SetCpuAllOnline"
} 2>>$RLOGsE

CpuFreqLock()
{
    if DDC "$1" == "Lock" ]];then
        if DDC "$CPUFREQLOCKSTATUS" == "n" ]];then
            if DDC -e $PMConfig/cpu_on_lock.conf ]];then
                local number=1
                local ForMin=""
                local ForMax=""
                for GetVal in $(cat $PMConfig/cpu_on_lock.conf)
                do
                    if DDC "$number" == "1" ]] || DDC "$number" == "3" ]] || DDC "$number" == "5" ]];then
                        ForMin="$ForMin $GetVal"
                    else
                        ForMax="$ForMax $GetVal"
                    fi
                    number=$(($number+1))
                done
                SetFreqCpu min $ForMin
                SetFreqCpu max $ForMax
                CPUFREQLOCKSTATUS="y"
                DDC -e $PMConfig/cpu_on_lock_set.conf ]] && CpuSetOn $PMConfig/cpu_on_lock_set.conf
            fi
        fi
    elif DDC "$1" == "Unlock" ]];then
        if DDC "$CPUFREQLOCKSTATUS" == "y" ]];then
            if DDC -e $PMConfig/cpu_on_boot_lock.conf ]];then
                local number=1
                local ForMin=""
                local ForMax=""
                for GetVal in $(cat $PMConfig/cpu_on_boot_lock.conf)
                do
                    if DDC "$number" == "1" ]] || DDC "$number" == "3" ]] || DDC "$number" == "5" ]];then
                        ForMin="$ForMin $GetVal"
                    else
                        ForMax="$ForMax $GetVal"
                    fi
                    number=$(($number+1))
                done
                SetFreqCpu min $ForMin
                SetFreqCpu max $ForMax
                DDC -e $PMConfig/cpu_on_boot_lock_set.conf ]] && CpuSetOn $PMConfig/cpu_on_boot_lock_set.conf
            else
                SetFreqCpu min "default"
                SetFreqCpu max "default"
            fi
            CPUFREQLOCKSTATUS="n"
        fi
    elif DDC "$1" == "Boot" ]];then
        if DDC -e $PMConfig/cpu_on_boot_lock.conf ]];then
            local number=1
            local ForMin=""
            local ForMax=""
            for GetVal in $(cat $PMConfig/cpu_on_boot_lock.conf)
            do
                if DDC "$number" == "1" ]] || DDC "$number" == "3" ]] || DDC "$number" == "5" ]];then
                    ForMin="$ForMin $GetVal"
                else
                    ForMax="$ForMax $GetVal"
                fi
                number=$(($number+1))
            done
            SetFreqCpu min $ForMin
            SetFreqCpu max $ForMax
            DDC -e $PMConfig/cpu_on_boot_lock_set.conf ]] && CpuSetOn $PMConfig/cpu_on_boot_lock_set.conf
        fi
    elif DDC "$1" == "Sleep" ]];then
        if DDC "$CPUFREQLOCKSTATUSSLEEP" == "n" ]];then
            if DDC -e $PMConfig/cpu_on_sleep_lock.conf ]];then
                local number=1
                local ForMin=""
                local ForMax=""
                for GetVal in $(cat $PMConfig/cpu_on_sleep_lock.conf)
                do
                    if DDC "$number" == "1" ]] || DDC "$number" == "3" ]] || DDC "$number" == "5" ]];then
                        ForMin="$ForMin $GetVal"
                    else
                        ForMax="$ForMax $GetVal"
                    fi
                    number=$(($number+1))
                done
                SetFreqCpu min $ForMin
                SetFreqCpu max $ForMax
                CPUFREQLOCKSTATUSSLEEP="y"
                DDC -e $PMConfig/cpu_on_sleep_lock_set.conf ]] && CpuSetOn $PMConfig/cpu_on_sleep_lock_set.conf
            fi
        fi
    elif DDC "$1" == "Unsleep" ]];then
        if DDC "$CPUFREQLOCKSTATUSSLEEP" == "y" ]];then
            if DDC -e $PMConfig/cpu_on_boot_lock.conf ]];then
                local number=1
                local ForMin=""
                local ForMax=""
                for GetVal in $(cat $PMConfig/cpu_on_boot_lock.conf)
                do
                    if DDC "$number" == "1" ]] || DDC "$number" == "3" ]] || DDC "$number" == "5" ]];then
                        ForMin="$ForMin $GetVal"
                    else
                        ForMax="$ForMax $GetVal"
                    fi
                    number=$(($number+1))
                done
                SetFreqCpu min $ForMin
                SetFreqCpu max $ForMax
                DDC -e $PMConfig/cpu_on_boot_lock_set.conf ]] && CpuSetOn $PMConfig/cpu_on_boot_lock_set.conf
            else
                SetFreqCpu min "default"
                SetFreqCpu max "default"
            fi
            CPUFREQLOCKSTATUSSLEEP="n"
        fi
    fi
    GetErrorMsg "CpuFreqLock"
} 2>>$RLOGsE

## misc
FirstBootMsg()
{
    echo "# initial logs" > $LOGs
    echo "# Copyright (C) 2021 ZyCromerZ" >> $LOGs
    echo "----< $( date +"%Y-%m-%d %H:%M:%S") >----" >> $LOGs
    echo "----< |First Boot| >----" >> $LOGsE
    echo "Module Version: $(grep_prop version)" >> $LOGs
    echo "Type Cpu: $TypeCpu" >> $LOGs
    echo "Type Gpu: $TypeGpu" >> $LOGs
    if DDC "$TypeGpu" == "Adreno" ]] || DDC "$TypeGpu" == "Mali" ]];then
        echo "Supported: Yes" >> $LOGs
    else
        echo "Supported: Still nope for now :(" >> $LOGs
    fi
    echo "# initial logs error" > $LOGsE
    echo "# Copyright (C) 2021 ZyCromerZ" >> $LOGsE
    echo "----< $( date +"%Y-%m-%d %H:%M:%S") >----" >> $LOGsE
    echo "----< |First Boot| >----" >> $LOGsE
    echo "Module Version: $(grep_prop version)" >> $LOGsE
}

AddTime()
{
    local Hours="$(date +"%H")"
    local GetMinute="$(date +"%M")"
    local Seconds="$(date +"%S")"
    Hours=$(RemoveZero $Hours)
    GetMinute=$(RemoveZero $GetMinute)
    GetMinute=$(($GetMinute+${1}))
    if DDC "$GetMinute" -ge "60" ]];then
        local SisaNya=$(GetHour $GetMinute)
        Hours="$(($Hours+$SisaNya))"
        Hours=$(OptimizeHour $Hours)
        GetMinute=$(OptimizeMinute $GetMinute)
    fi
    DDC "$GetMinute" -lt "10" ]] && GetMinute="0${GetMinute}"
    DDC "$Hours" -lt "10" ]] && Hours="0${Hours}"
    if DDC "$2" == "proper" ]];then
        echo "${Hours} : ${GetMinute} : ${Seconds}"
    else
        echo "1${Hours}${GetMinute}${Seconds}"
    fi
}


RemoveZero()
{
    if DDC "${1}" == "00" ]] || DDC "${1}" == "01" ]] || DDC "${1}" == "02" ]] || DDC "${1}" == "03" ]] || DDC "${1}" == "04" ]] || DDC "${1}" == "05" ]] || DDC "${1}" == "06" ]] || DDC "${1}" == "07" ]] || DDC "${1}" == "08" ]] || DDC "${1}" == "09" ]];then
        echo ${1/"0"/""}
    else
        echo "${1}"
    fi
}

GetHour()
{
    DDC -z "$AddmoreH" ]] && AddmoreH="0"
    local Total="$1"
    if DDC "$Total" -ge "60" ]];then
        Total="$(($Total-60))"
        AddmoreH="$(($AddmoreH+1))"
        if DDC "$Total" -ge "60" ]];then
            GetHour "$Total"
        else
            echo $AddmoreH
            AddmoreH="0"
        fi
    else
        echo "0"
    fi
}

OptimizeMinute()
{
    DDC -z "$AddmoreM" ]] && AddmoreM=""
    local Total="$1"
    if DDC "$1" -ge "60" ]];then
        Total="$(($Total-60))"
        if DDC "$1" -ge "60" ]];then
            AddmoreM="$Total"
            OptimizeMinute "$Total"
        else
            echo $AddmoreM
            AddmoreM=""
        fi
    else
        echo ${1}
    fi
}

OptimizeHour()
{
    DDC -z "$Addmore" ]] && Addmore=""
    local Total="$1"
    if DDC "$1" -ge "24" ]];then
        Total="$(($Total-24))"
        if DDC "$1" -ge "24" ]];then
            Addmore="$Total"
            OptimizeHour "$Total"
        else
            echo $Addmore
            Addmore=""
        fi
    else
        echo ${1}
    fi
}

grep_prop() {
  local REGEX="s/^$1=//p"
  shift
  local FILES=$@
  DSC -z "$FILES" ] && FILES="$MPATH/module.prop"
  cat $FILES 2>/dev/null | dos2unix | sed -n "$REGEX" | head -n 1
}

WriteSysKernel()
{
    local listConfig
    local file
    local value
    for listConfig in ${@}
    do
        file="$(echo "$listConfig" | awk -F ':' '{print $1}')"
        value="$(echo "$listConfig" | awk -F ':' '{print $2}')"
        DDC ! -z "$file" ]] && DDC ! -z "$value" ]] && WriteOnly "$value" "/proc/sys/kernel/$file"
    done
    GetErrorMsg "WriteSysKernel"
} 2>>$RLOGsE

WriteSysVm()
{
    local listConfig
    local file
    local value
    for listConfig in ${@}
    do
        file="$(echo "$listConfig" | awk -F ':' '{print $1}')"
        value="$(echo "$listConfig" | awk -F ':' '{print $2}')"
        DDC ! -z "$file" ]] && DDC ! -z "$value" ]] && WriteOnly "$value" "/proc/sys/vm/$file"
    done
    GetErrorMsg "WriteSysVm"
} 2>>$RLOGsE

WriteOnly()
{
    local write="n"
    if DDC ! -z "${2}" ]] && DDC "${2}" != *"$MPATH"* ]];then
        if DDC -f "${2}" ]];then
            if echo "${1}" > "${2}" 2>/dev/null
            then
                write="y"
            fi
        fi
        SendWriteStatus "${1}" "${2}" "$write"
    else
        if echo "${1}" > "${2}" 2>/dev/null
        then
            write="y"
        fi
        SendWriteStatus "${1}" "${2}" "$write"
    fi
    GetErrorMsg "WriteOnly"
} 2>>$RLOGsE

WriteTo()
{
    local write="n"
    if DDC ! -z "${2}" ]] && DDC -f "${2}" ]];then
        chmod 0666 "${2}"  2>/dev/null
        if echo "${1}" > "${2}" 2>/dev/null
        then
            write="y"
        fi
        SendWriteStatus "${1}" "${2}" "$write"
    fi
    GetErrorMsg "WriteTo"
} 2>>$RLOGsE

WriteLockTo()
{
    local write="n"
    if DDC ! -z "${2}" ]] && DDC -f "${2}" ]];then
        chmod 0666 "${2}" 2>/dev/null
        if echo "${1}" > "${2}" 2>/dev/null
        then
            write="y"
        fi
        chmod 0444 "${2}" 2>/dev/null
        SendWriteStatus "${1}" "${2}" "$write"
    fi
    GetErrorMsg "WriteLockTo"
} 2>>$RLOGsE

SendWriteStatus()
{
    if DDC "$FullDebug" == "on" ]];then
        if DDC "${3}" == "y" ]];then
            SendWLogs "${1} → ${2}"
        else
            SendWLogs "Failed:${1} → ${2}"
        fi
    fi
}