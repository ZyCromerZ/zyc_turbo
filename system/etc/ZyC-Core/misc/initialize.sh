#!/system/bin/sh
#
# Copyright (C) 2021 ZyCromerZ
# SPDX-License-Identifier: GPL-3.0-or-later
#
GetBusyBox="none"
alias DSC="["
alias DDC="[["
sleep 1s
for i in  /system/xbin /system/bin /sbin /su/xbin /data/adb/modules/busybox-ndk/system/xbin /data/adb/modules_update/busybox-ndk/system/xbin /data/adb/modules/busybox-ndk/system/bin /data/adb/modules_update/busybox-ndk/system/bin
do
    if [[ "$GetBusyBox" == "none" ]]; then
        if [[ -f $i/busybox ]]; then
            GetBusyBox=$i/busybox
        fi
    fi
done

if [[ "$GetBusyBox" == "none" ]];then
    GetBusyBox=""
else
    for ListCmds in $($GetBusyBox --list)
    do
        if [[ "$ListCmds" == "[" ]];then
            alias "DSC"="$GetBusyBox $ListCmds"
        elif [[ "$ListCmds" == "[[" ]];then
            alias "DDC"="$GetBusyBox $ListCmds"
        else
            alias "$ListCmds"="$GetBusyBox $ListCmds"
        fi
    done
fi

## just get somethings
MiD="$(cat /system/etc/ZyC-Core/info/modules_id.info)"
MPATH="$(cat /system/etc/ZyC-Core/info/magisk_path)/$MiD"
PMConfig="$MPATH/system/etc/ZyC-Core/configs"
LOGs=$MPATH/system/etc/ZyC-Core/info/logs.log
LOGsE=$MPATH/system/etc/ZyC-Core/info/logs_error.log
RLOGsE=$MPATH/system/etc/ZyC-Core/info/logs_error.log
# MODULE_STATUS="$(cat $MPATH/system/etc/ZyC-Core/configs/status.conf)"
MODULE_STATUS="1"

## get gpu type
MALIGPU="n"
TypeGpu='Undetected / Unknow'
if DDC -d /sys/class/kgsl/kgsl-3d0 ]]; then
    NyariGPU="/sys/class/kgsl/kgsl-3d0"
    TypeGpu="Adreno"
elif DDC -d /sys/devices/platform/kgsl-3d0.0/kgsl/kgsl-3d0 ]]; then
    NyariGPU="/sys/devices/platform/kgsl-3d0.0/kgsl/kgsl-3d0"
    TypeGpu="Adreno"
elif DDC -d /sys/devices/soc/*.qcom,kgsl-3d0/kgsl/kgsl-3d0 ]]; then
    NyariGPU="/sys/devices/soc/*.qcom,kgsl-3d0/kgsl/kgsl-3d0"
    TypeGpu="Adreno"
elif DDC -d /sys/devices/soc.0/*.qcom,kgsl-3d0/kgsl/kgsl-3d0 ]]; then
    NyariGPU="/sys/devices/soc.0/*.qcom,kgsl-3d0/kgsl/kgsl-3d0"
    TypeGpu="Adreno"
elif DDC -d /sys/devices/platform/*.gpu/devfreq/*.gpu ]]; then
    NyariGPU=/sys/devices/platform/*.gpu/devfreq/*.gpu
elif DDC -d /sys/devices/platform/*.mali ]]; then
    NyariGPU=/sys/devices/platform/*.mali
    MALIGPU="y"
    TypeGpu="Mali"
elif DDC -d /sys/class/misc/mali0 ]]; then
    NyariGPU=/sys/class/misc/mali0
    MALIGPU="y"
    TypeGpu="Mali"
else
    NyariGPU='';
fi

TypeCpu="$(cat /proc/cpuinfo | grep Hardware | awk -F ": " '{print $2}')"
# if [[ "$TypeCpu" == *"Qualcomm Technologies, Inc"* ]];then
#     TypeCpu=${TypeCpu/"Qualcomm Technologies, Inc "/""}
# fi
# if [[ "$TypeCpu" == *"/"* ]];then
#     TypeCpu="$(echo $TypeCpu | awk -F "/" '{print $1}')"
# fi

## misc
GheymingMode="n"
GoTurbo="0"
GoNormal="0"
StopSpamSendInfo="n"
StopScanGameList="n"
LastDropCache=""
ScreenState="on"
CurrentDisplayStatus="on"
FullDebug="on"
DoSilentWrite="0"
SwitchForceDoze="0"
CpuChangeFail="0"
CPUFREQLOCKSTATUS="n"
CPUFREQLOCKSTATUSSLEEP="n"