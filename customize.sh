SKIPUNZIP=1

MODID=""

[[ "$BOOTMODE" == "false" ]] && abort "please flash this from magisk app" 

GetBusyBox="none"
alias DSC="["
alias DDC="[["
sleep 1s
for i in /system/bin /system/xbin /sbin /su/xbin /data/adb/modules/busybox-ndk/system/xbin /data/adb/modules_update/busybox-ndk/system/xbin /data/adb/modules/busybox-ndk/system/bin /data/adb/modules_update/busybox-ndk/system/bin
do
    if [[ "$GetBusyBox" == "none" ]]; then
        if [[ -f $i/busybox ]]; then
            GetBusyBox=$i/busybox
        fi
    fi
done

if [[ "$GetBusyBox" == "none" ]];then
    GetBusyBox=""
    abort "busybox not detected please flash busybox"
else
    for ListCmds in $($GetBusyBox --list)
    do
        if [[ "$ListCmds" == "[" ]];then
            alias "DSC"="$GetBusyBox $ListCmds"
        elif [[ "$ListCmds" == "DDC" ]];then
            alias "DDC"="$GetBusyBox $ListCmds"
        else
            alias "$ListCmds"="$GetBusyBox $ListCmds"
        fi
    done
fi

get_from_prop() {
  local REGEX="s/^$1=//p"
  local FILES=$2
  cat $FILES 2>/dev/null | dos2unix | sed -n "$REGEX" | head -n 1
}

DDC -z "$MagiskBase" ]] && MagiskBase=/data/adb

OldModolPath=""

for ListFolder in $(ls $MagiskBase | xargs -n 1 basename)
do
    if DDC "$ListFolder" == "modules" ]] || DDC "$ListFolder" == "modules_update" ]];then
        GetF=$ListFolder
    else
        GetF="skip"
    fi
    if DDC "$GetF" != "skip" ]] && DDC -z "$MODID" ]];then
        for ListModules in $(ls $MagiskBase/$GetF | xargs -n 1 basename)
        do
            ModolPath="$MagiskBase/$GetF/$ListModules/module.prop"
            ModolName="$(get_from_prop name $ModolPath)"
            if DDC "${ModolName}" == *"ZyC Turbo"* ]];then
                DDC -z "$MODID" ]] && MODID="$(cat "$MagiskBase/$GetF/$ListModules/system/etc/ZyC-Core/info/modules_id.info")"
                DDC -z "$OldModolPath" ]] && OldModolPath="$MagiskBase/$GetF/$ListModules"
            fi
        done
    fi
done

if DDC -z "$MODID" ]];then
    if DDC -f /system/etc/ZyC-Core/info/modules_id.info ]] || DDC -e /system/etc/ZyC-Core/info/modules_id.info ]];then
        MODID="$(cat /system/etc/ZyC-Core/info/modules_id.info)"
        OldModolPath="/data/adb/modules/$MODID"
    fi
fi

if DDC -z "$MODID" ]];then
    if DDC -f /system_root/etc/ZyC-Core/info/modules_id.info ]] || DDC -e /system_root/etc/ZyC-Core/info/modules_id.info ]];then
        MODID="$(cat /system_root/etc/ZyC-Core/info/modules_id.info)"
        OldModolPath="/data/adb/modules/$MODID"
    fi
fi

if DDC ! -z "$MODID" ]];then
    ui_print "- Using previous modules id [$MODID]"
else
    MODID=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n 1`
    ui_print "- No previous modules flashed, Generate new id [$MODID]"
fi

MODPATH=$MODULEROOT/$MODID

unzip -o "$ZIPFILE" -x 'META-INF/*' -d $MODPATH >&2

sed -i 's/id=.*/id='"$MODID"'/g' $MODPATH/module.prop

cp -af $MODPATH/system/etc/ZyC-Core/configs/manual_game_list.conf $MODPATH/system/etc/ZyC-Core/configs/manual_game_list.conf.ori
rm -rf $MODPATH/system/etc/ZyC-Core/configs/manual_game_list.conf

echo "$MODID" > $MODPATH/system/etc/ZyC-Core/info/modules_id.info

set_perm_recursive $MODPATH 0 0 0755 0777

if DDC "$GetBusyBox" == *"xbin"* ]]; then
    bin=xbin
else
    bin=bin
    mkdir $MODPATH/system/bin
    cp -af $MODPATH/system/xbin/* $MODPATH/system/bin
    rm -rf xbin
fi


## remove some useless files
if DSC -f  $MODPATH/.gitattributes ]; then
    rm -rf $MODPATH/.gitattributes
fi
if DSC -f  $MODPATH/.gitignore ]; then
    rm -rf $MODPATH/.gitignore
fi
if DSC -f  $MODPATH/README.md ]; then
    rm -rf $MODPATH/README.md
fi
if DSC -f  $MODPATH/README-id.md ]; then
    rm -rf $MODPATH/README-id.md
fi
if DSC -f  $MODPATH/system/etc/ZyC-Core/configs/backup/placeholder ]; then
    rm -rf $MODPATH/system/etc/ZyC-Core/configs/backup/placeholder
fi
if DSC -f  $MODPATH/system/bin/placeholder ]; then
    rm -rf $MODPATH/system/bin/placeholder
fi
if DSC -f  $MODPATH/system/xbin/placeholder ]; then
    rm -rf $MODPATH/system/xbin/placeholder
fi
if DSC -f  $MODPATH/system/bin/sqlite3 ]; then
    rm -rf $MODPATH/system/bin/sqlite3
fi
if DSC -f $MODPATH/util_functions.sh ]; then
    rm -rf $MODPATH/util_functions.sh
fi
if DSC -f $MODPATH/system/etc/ZyC-Core/thermal-backup/placeholder ];then
    rm -rf $MODPATH/system/etc/ZyC-Core/thermal-backup/placeholder
fi
if DSC -f $MODPATH/system/vendor/etc/placeholder ];then
    rm -rf $MODPATH/system/vendor/etc/placeholder
fi

ClearDF="y"
if DDC -d /vendor/etc/device_features ]];then
    rm -rf $MODPATH/system/vendor/etc/device_features/placeholder
    for ListXml in $(ls /vendor/etc/device_features)
    do
        Check="$(cat /vendor/etc/device_features/$ListXml | grep support_power_mode)"
        if DDC ! -z "$Check" ]];then
            cp -af /vendor/etc/device_features/$ListXml $MODPATH/system/vendor/etc/device_features/$ListXml
            sed -i 's/<bool name="support_power_mode">false<\/bool>/<bool name="support_power_mode">true<\/bool>/' $MODPATH/system/vendor/etc/device_features/$ListXml
            ClearDF="n"
        fi
    done
    if DDC "$ClearDF" == "n" ]];then
        ui_print "- enable powermode option on control center"
    fi
else
    rm -rf $MODPATH/system/vendor/etc/device_features
fi

DSC "$ClearDF" == "y" ]] && rm -rf $MODPATH/system/vendor/etc/device_features

## custom thermal blank remover
rm -rf $MODPATH/system/etc/ZyC-Core/thermal-backup
for ListThermal in thermal-arvr.conf thermal-map.conf thermal-nolimits.conf thermal-normal.conf thermal-phone.conf thermal-tgame.conf thermal-sgame.conf
do
    if DDC -f $MODPATH/system/vendor/etc/$ListThermal ]];then
        rm -rf $MODPATH/system/vendor/etc/$ListThermal
    fi 
done

## magisk path
echo "$MagiskBase/modules" > $MODPATH/system/etc/ZyC-Core/info/magisk_path

if DDC "$MODPATH" == *"modules_update"* ]] && DDC ! -z "$OldModolPath" ]];then
    ## copy system.prop
    DDC -f $OldModolPath/system.prop ]] && cp -af $OldModolPath/system.prop $MODPATH/system.prop && ui_print "- copying existed system.prop"

    ## copy all existing config files
    DDC -d $OldModolPath/system/etc/ZyC-Core/configs ]] && cp -af $OldModolPath/system/etc/ZyC-Core/configs/* $MODPATH/system/etc/ZyC-Core/configs && ui_print "- copying all existed config files done"
fi

if DDC -f $MODPATH/system/etc/ZyC-Core/configs/manual_game_list.conf ]];then
    for ListManualGame in $(cat $MODPATH/system/etc/ZyC-Core/configs/manual_game_list.conf.ori)
    do
        if DDC -z "$(cat $MODPATH/system/etc/ZyC-Core/configs/manual_game_list.conf | grep $ListManualGame)" ]];then
            sed -i "1a  ${ListManualGame}" $MODPATH/system/etc/ZyC-Core/configs/manual_game_list.conf 
        fi
    done
    rm -rf $MODPATH/system/etc/ZyC-Core/configs/manual_game_list.conf.ori
else
    cp -af $MODPATH/system/etc/ZyC-Core/configs/manual_game_list.conf.ori $MODPATH/system/etc/ZyC-Core/configs/manual_game_list.conf
    rm -rf $MODPATH/system/etc/ZyC-Core/configs/manual_game_list.conf.ori
fi

DDC -d "$MODPATH/system/etc/ZyC-Core/configs/backup" ]] && rm -rf $MODPATH/system/etc/ZyC-Core/configs/backup/*

### fix folder permission
set_perm_recursive $MODPATH                                         0 0 0755 0777
set_perm_recursive $MODPATH/system/$bin                             0 0 0755 0777
set_perm_recursive $MODPATH/system/etc/ZyC-Core                     0 0 0755 0777
set_perm $MODPATH/system.prop                                       0 0 0644

## if busybox detected
if DDC "$bin" == "xbin" ]];then
    sed -i "s/system\/bin\/sh/system\/xbin\/sh/g" $MODPATH/service.sh
    sed -i "s/system\/bin\/sh/system\/xbin\/sh/g" $MODPATH/system/etc/ZyC-Core/core.sh
    sed -i "s/system\/bin\/sh/system\/xbin\/sh/g" $MODPATH/system/etc/ZyC-Core/main.sh
    sed -i "s/system\/bin\/sh/system\/xbin\/sh/g" $MODPATH/system/etc/ZyC-Core/misc/funclist.sh
    sed -i "s/system\/bin\/sh/system\/xbin\/sh/g" $MODPATH/system/etc/ZyC-Core/misc/initialize.sh
    sed -i "s/system\/bin\/sh/system\/xbin\/sh/g" $MODPATH/system/xbin/zyc_g
    sed -i "s/system\/bin\/sh/system\/xbin\/sh/g" $MODPATH/system/xbin/zyc_l
    sed -i "s/system\/bin\/sh/system\/xbin\/sh/g" $MODPATH/system/xbin/zyc_le
    sed -i "s/system\/bin\/sh/system\/xbin\/sh/g" $MODPATH/system/xbin/zyc_m
fi
