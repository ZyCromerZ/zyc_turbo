## link download
go here : https://www.pling.com/p/1589922/
## Info Modules
Modul ini bekerja ketika penggunakan GPU sedang tinggi atau berdasarkan app tertentu
<br>
Letak Configurasi : <code>/data/adb/modules/(Id-Module)/system/etc/ZyC-Core/configs</code> <br>
dan jangan sentuh / ubah file apapun di dalam folder <code>/data/adb/modules/(Id-Module)/system/etc/ZyC-Core/configs/backup</code>

## Info Config
* game_list.conf
    * file yg ada secara otomatis berdasarkan aplikasi yg ter install untuk mengubah mode modules berdasarkan app tertentu ketika dibuka
* manual_game_list.conf
    * list game yg bisa di deteksi untuk generated game_list.conf
* max_check_gpu_usage.conf
    * Jumlah cek untuk mengubah mode ke enable atau disable
* max_gpu_usage.conf
    * maksimal gpu usage untuk membuat app saat ini di deteksi sebagai game (buat enable tweaknya)
* min_gpu_usage.conf
    * minimum gpu usage untuk membuat app saat ini di deteksi sebagai app normal(buat disable tweaknya)
* wait_when_off.conf
    * waktu tunggu ketika tweak di mode disabled sebelom melakukan check lagi
* wait_when_on.conf
    * waktu tunggu ketika tweak di mode enabled sebelom melakukan check lagi
* modules_mode.conf
    * info tentank mode modulnya (auto|force on|force off|by app|by GPU usage)
* sconfig.thermal.conf
    * thermal ketika modules nyala: info mode
        * 0 = Normal
        * 9 = Game
        * 16 = Game V2
        * -1 = disable cpu thermal
        * other mode : 1,2,8,10,11,12,13,14,15
* sconfig.thermal.lock
    * lock sthermal config
* scheduler.conf
    * ganti scheduler (kaya cfq bfq noop etc) pas module nyala
* scheduler.lock.conf
    * lock scheduler value
* show_error.conf
    * to melihat/menyembunyikan pesan error (recomended biarin off ae)
* mtk_cpu_mode_on.conf (buat modul pas on) & mtk_cpu_mode_on.conf (buat modul pas off) (cuma buat MTK)
    * buat rubah mode kernel dari hybrid(default one) ke EAS / HMP kalo kernel nya support, and isinya
        * 0 = HMP
        * 1 = EAS
        * 2 = hybrid
* drop_caches.conf
    * config buat nyalain fitur otomatis hapus cache ram, caranya kirim character tertentu ke /proc/sys/vm/drop_caches, isinya
        * 0 = off
        * 1 = hapus clear pageCache only.
        * 2 = hapus clear dentries ama inodes.
        * 3 = hapus clear pagecache, dentries, ama inodes.
* drop_caches_time.conf
    * config buat waktu nunggu buat otomatis hapus cache, berdasarkan menit (bawaan nya 60 menit)
* force_doze.conf
    * paksa ke mode doze ketika layarnya mati
* sultan_pid_app.conf
    * list id app buat ganti mode sultan pid (ini cuma buat kernel yg saya compile untuk devices vayu(Poco X3 Pro) saja)
    * intinya jangan pernah di ubah list app dalem ini
* use_cpu_tweak.conf
    * kalo anda mau pake beberapa tweak cpu, tinggal set ke 1
* silent_overwrite.conf
    * kalo situ pake modol tweak laennya, teros jadi conflict, cobain set ke 1, default 0 
* write_info.conf
    * untuk melihat/menyembunyikan status tulis
* status.conf
    * untuk mematikan module (default 1)

## Info Terminal Command
* zyc_l
    * untuk melihat logs
* zyc_le
    * untuk melihat logs error
* zyc_ldb
    * untuk melihat logs hasil optimize db file
* zyc_g
    * untuk update list game yg terdeteksi
* zyc_m
    * untuk mengubah mode modules (auto|force on|force off|by app|by GPU usage)
* zyc_mlbb
    * untuk mengubah pengaturan grafis mlbb
* zyc_fsync
    * untuk mengubah mode fsync
* zyc_s
    * untuk mengubah file config
* zyc_mc
    * untuk mengubah frequensi CPU di device MTK
* zyc_cf
    * untuk melihat info frequensi CPU