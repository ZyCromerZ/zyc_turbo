## link download
go here : https://www.pling.com/p/1589922/
## Info Modules
This module tweak will be turned on when high gpu usage detected / while open some games
<br>
Config Path : <code>/data/adb/modules/(Module-id)/system/etc/ZyC-Core/configs</code> <br>
and please dont touch / edit any files inside folder <code>/data/adb/modules/(Module-id)/system/etc/ZyC-Core/configs/backup</code>

## Info Config
* game_list.conf
    * automatic generated depend installed app for turn on modules when specific app opened
* manual_game_list.conf
    * list game can be detected for generated game_list.conf
* max_check_gpu_usage.conf
    * Total check time to change mode to enable or disable tweak
* max_gpu_usage.conf
    * maximumg gpu usage to make current proccess detected as game (i mean to enable tweak)
* min_gpu_usage.conf
    * minimum gpu usage to make current proccess detected as normal app (i mean to disable tweak)
* wait_when_off.conf
    * wait time when tweak disabled before do check again
* wait_when_on.conf
    * wait time when tweak enabled before do check again
* modules_mode.conf
    * info about modules mode (auto|force on|force off|by app|by GPU usage)
* sconfig.thermal.conf
    * thermal mode when modules on: info mode
        * 0 = Normal
        * 9 = Game
        * 16 = Game V2
        * -1 = disable cpu thermal
        * other mode : 1,2,8,10,11,12,13,14,15
* sconfig.thermal.lock.conf
    * lock sthermal config
* scheduler.conf
    * change scheduler (like cfq bfq noop etc) when module on
* scheduler.lock.conf
    * lock scheduler value
* show_error.conf
    * to show/hide error message (recomended to leave it off)
* mtk_cpu_mode_on.conf (while modules tweak on) & mtk_cpu_mode_on.conf (while modules tweak off) (for MTK only)
    * changes kernel mode from hybrid(default one) to EAS / HMP if kernels supported it, and value about it
        * 0 = HMP
        * 1 = EAS
        * 2 = hybrid
* drop_caches.conf
    * config to enable auto clear ram cache, using method send some value to /proc/sys/vm/drop_caches ,value about it
        * 0 = off
        * 1 = clear pageCache only.
        * 2 = clear dentries and inodes.
        * 3 = clear pagecache, dentries, and inodes.
* drop_caches_time.conf
    * config for wait time to auto clear ram cache, by minutes (default 60 minutes)
* force_doze.conf
    * forcing to doze idle state when display/screen off
* sultan_pid_app.conf
    * list app id that will be change sultan pid mode (this thing only for my compiled kernels for vayu(Poco X3 Pro) devices only)
    * recomended to not change list app on it
* use_cpu_tweak.conf
    * if u want to apply some cpu tweak from this modules, just set it to 1
* silent_overwrite.conf
    * if u use another tweak modules, then its conflict, try set this to 1, default 0
* write_info.conf
    * to show/hide write status 
* status.conf
    * for turn off module (default 1)

## Info Terminal Command
* zyc_l
    * for check logs
* zyc_le
    * for check logs error
* zyc_ldb
    * for check logs optimize db file result
* zyc_g
    * for update detected game list
* zyc_m
    * for change modules mode (auto|force on|force off|by app|by GPU usage)
* zyc_mlbb
    * for change mlbb graphic option
* zyc_fsync
    * for change fsync mode
* zyc_s
    * for update config file
* zyc_mc
    * for change CPU frequency on MTK devices
* zyc_cf
    * for check CPU frequency info