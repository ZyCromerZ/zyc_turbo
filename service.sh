#!/system/bin/sh
#
# Copyright (C) 2021 ZyCromerZ
# SPDX-License-Identifier: GPL-3.0-or-later
#
## waiting booting done

while [[ `getprop sys.boot_completed` -ne 1 ]] && [[ ! -d "/sdcard" ]] && [[ ! -d /system/bin ]] && [[ ! -f /system/etc/ZyC-Core/info/modules_id.info ]]
do
       sleep 1
done

GetBusyBox="none"
GetBusyBoxPath="none"
for i in /system/xbin /system/bin /sbin /su/xbin /data/adb/modules/busybox-ndk/system/xbin /data/adb/modules_update/busybox-ndk/system/xbin /data/adb/modules/busybox-ndk/system/bin /data/adb/modules_update/busybox-ndk/system/bin; do
    if [[ "$GetBusyBox" == "none" ]]; then
        if [[ -f $i/busybox ]]; then
            GetBusyBox=$i/busybox
            GetBusyBoxPath="$1"
            break
        fi
    fi
done

[[ "$GetBusyBox" == "none" ]] && exit

MiD=""
GetMid()
{
    if [[ -f /system/etc/ZyC-Core/info/modules_id.info ]];then
        if [[ -z "$MiD" ]];then
            MiD="$(cat /system/etc/ZyC-Core/info/modules_id.info)"
            ModulPath="$(cat /system/etc/ZyC-Core/info/magisk_path)/$MiD"
        fi
    fi
    [[ -z "$MiD" ]] && sleep 1s && GetMid
}
GetMid

TotalTime="30"

for GetTime in 5 5 5 5 1 1 1 1 1 1 1 1 1 1
do
    echo "modules will run after $TotalTime s" > $ModulPath/system/etc/ZyC-Core/info/logs.log
    echo "modules will run after $TotalTime s" > $ModulPath/system/etc/ZyC-Core/info/logs_error.log
    sleep ${GetTime}s
    TotalTime=$(($TotalTime-$GetTime))
done

$GetBusyBox sh $ModulPath/system/etc/ZyC-Core/core.sh 'BOOTmode="1"' ModulPath="'$(cat /system/etc/ZyC-Core/info/magisk_path)/$MiD'" &